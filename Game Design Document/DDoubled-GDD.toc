\contentsline {section}{\numberline {1}Changelog}{3}{section.1}
\contentsline {section}{\numberline {2}Overview}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Game Design}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Key Features}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Future Plans}{4}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Story}{5}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}Beginning}{5}{subsubsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.2}Progression}{5}{subsubsection.2.4.2}
\contentsline {subsubsection}{\numberline {2.4.3}Ending}{5}{subsubsection.2.4.3}
\contentsline {section}{\numberline {3}Gameplay}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Overview}{6}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Character}{6}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Dimensions}{6}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}Interface}{7}{subsubsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.4}Menu}{7}{subsubsection.3.1.4}
\contentsline {subsection}{\numberline {3.2}User Skills}{8}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Game Mechanics}{9}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Map and Levels}{12}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Progression and Challenge}{12}{subsection.3.5}
\contentsline {subsubsection}{\numberline {3.5.1}FlowChart}{13}{subsubsection.3.5.1}
\contentsline {subsubsection}{\numberline {3.5.2}Player Experience}{14}{subsubsection.3.5.2}
\contentsline {subsection}{\numberline {3.6}Saving}{14}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Losing}{14}{subsection.3.7}
\contentsline {section}{\numberline {4}Art Style}{15}{section.4}
\contentsline {subsection}{\numberline {4.1}Art Bible}{15}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Concept Art Overview}{15}{subsection.4.2}
\contentsline {section}{\numberline {5}Animations}{16}{section.5}
\contentsline {subsection}{\numberline {5.1}Animation Types}{16}{subsection.5.1}
\contentsline {section}{\numberline {6}Music and Sounds}{17}{section.6}
\contentsline {subsection}{\numberline {6.1}Sound Styles}{17}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Sounds List}{17}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Musics List}{17}{subsection.6.3}
\contentsline {section}{\numberline {7}Technical Spec}{18}{section.7}
\contentsline {subsection}{\numberline {7.1}Development Platform and Tools}{18}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Game Engine}{18}{subsection.7.2}
\contentsline {subsubsection}{\numberline {7.2.1}Collision Detection}{18}{subsubsection.7.2.1}
\contentsline {subsection}{\numberline {7.3}Light}{18}{subsection.7.3}
\contentsline {subsection}{\numberline {7.4}Rendering System}{18}{subsection.7.4}
\contentsline {subsubsection}{\numberline {7.4.1}3D Rendering}{18}{subsubsection.7.4.1}
\contentsline {subsubsection}{\numberline {7.4.2}Camera}{18}{subsubsection.7.4.2}
\contentsline {section}{\numberline {8}Platform and Audience}{19}{section.8}
\contentsline {section}{\numberline {9}Similar Games}{19}{section.9}
\contentsline {section}{\numberline {10}Writing}{20}{section.10}
\contentsline {subsection}{\numberline {10.1}Script}{20}{subsection.10.1}
\contentsline {subsection}{\numberline {10.2}Game Tutorial and Manual}{20}{subsection.10.2}
\contentsline {subsubsection}{\numberline {10.2.1}System Requirements}{20}{subsubsection.10.2.1}
\contentsline {subsubsection}{\numberline {10.2.2}Installation Instructions}{20}{subsubsection.10.2.2}
\contentsline {subsubsection}{\numberline {10.2.3}How To Play}{21}{subsubsection.10.2.3}
\contentsline {section}{\numberline {11}Walkthrough}{22}{section.11}
\contentsline {subsection}{\numberline {11.1}Levels' Solutions}{22}{subsection.11.1}
