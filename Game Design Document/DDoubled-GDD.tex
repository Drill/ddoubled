\documentclass[10pt]{article}

\usepackage[margin=1in, paperwidth=8.5in, paperheight=11in]{geometry}
\usepackage{graphicx}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{xcolor}
\usepackage{hyperref}

\begin{document}
\sloppy

\begin{titlepage}
    \centering
    \vspace*{1mm}
    \vfill
    \line(1,0){400}\\[1cm]
    \huge{\textbf{DDoubled}}\\[3mm]
    \Large{\textbf{Game Design Document}}\\[0.5cm]
    \line(1,0){400}
    \vfill
    {\large v1.0 \par}
	{\large \today \par}  	
\end{titlepage}

\tableofcontents
\newpage

\section{Changelog}
% future changes go here

\newpage

\section{Overview}
\subsection{Game Design}
\textbf{\textit{DDoubled}} is a \textbf{fascinating puzzle game} in a \textbf{3D environment} for PC in which the player has to solve \textbf{interesting} puzzles controlling \textbf{multiple versions} of the character at the same time, who will act exactly in the same way.\\

Each level presents various \textbf{objects} and the player has to use them in order to reach and open the \textbf{door} that leads to the next level. These objects can be \textbf{moved}, \textbf{carried} or \textbf{activated}.\\
All puzzles have one thing in common: the presence of one (or more) \textbf{special item} that allows to create an \textbf{identical copy} of the character. Once the character has been doubled, the player will have to control all the versions with the \textbf{same inputs} at the \textbf{same time}.\\
The solutions of the levels are based on the ability of the player to manage all the characters, \textbf{separating} or \textbf{approaching} them \textbf{exploiting the environment}.

\subsection{Key Features}
\begin{itemize}
    \item 3D environment with isometric view
    \item 3 different worlds
    \item Different puzzles to solve with different strategies
    \item Multiple characters controlled at the same time
    \item Possibility to create copies of objects carried
\end{itemize}

\subsection{Future Plans}
\begin{itemize}
    \item A 4th world: \textit{Minimal Dimension}
    \item Customization of the character
    \item Hidden items

\end{itemize}

\newpage

\subsection{Story}
\subsubsection{Beginning}
The \textbf{main and only} character of the game is a \textbf{scientist} who is working at his \textbf{latest creation}: an object that allows to travel through \textbf{other dimensions}.\\
One day there is an \textbf{accident}: the object, \textbf{still in experimental phase}, is activated and it generates a vortex that sucks the scientist and the surrounding objects into it.

Then the scientist finds himself \textbf{somewhere else} and there is no trace of his laboratory or even of his city. There is only one thing familiar: the \textbf{other experiment} on which he was working. This one allows the scientist to create a copy of himself and its name is \textbf{\textit{DDouble}}. It will prove useful in this unknown place.

\subsubsection{Progression}
By resolving the levels, the scientist travels in the unknown place, exploring the created dimensions one by one. The story is linear, but it is possible to come back to previous dimensions.

\subsubsection{Ending}
Once finished the last dimension the game ends, but it is possible to revisit each level and play it.\\
\textit{Finishing} the game doesn't mean \textit{completing} it: some levels can be skipped and played later.

\newpage

\section{Gameplay}
\subsection{Overview}
In \textbf{\textit{DDoubled}} the player is able to control \textbf{two, three or even more} copies of the character at the same time, all responding to the same inputs.

The \textbf{goal} of each level is to use these copies and the objects in the scene in order to \textbf{solve the puzzles} and open and reach the door that leads to the next level.\\

There is \textbf{no difference} between \textit{``the original"} and \textit{``the copy"}: the player has to lead one of them to the door in order to advance in the game. In any case, each level starts with \textbf{just one} character.

\subsubsection{Character}
The game has \textbf{only one character}: the scientist.\\
His most incredible inventions are \textbf{\textit{DDouble}}, which creates an identical copy of who touches it, and the object that allows to \textbf{travel through dimensions} (still in experimental phase). He collects various objects and he exposes them in his laboratory.\\

His \textbf{clumsiness} is the responsible for the \textbf{accident} that brings him to another dimension before the completion of his experiment.

\subsubsection{Dimensions}
From a rum with the \textbf{pirates} to a \textbf{scary and dark} place.\\
From a \textbf{technological} city to a \textbf{minimalist} world.\\
This game will bring the player through \textbf{four} different dimensions, each of them based on an object in the scientist's room which has been sucked into the vortex:
\begin{itemize}
    \item \textbf{Pirate World}\\
    The forest, the beach, the sea and a pirate ship are the themes of the levels present here.
    \item \textbf{Ghost Kingdom}\\
    A dark and creepy dimension in which the light is barely present. A graveyard, a decadent house and a labyrinth are present.
    \item \textbf{Neon City}\\
    A futuristic city in which the neons are dominant. The adventure here takes place at night and both internal and external settings are explored.
    \item \textbf{Minimal Dimension} \textit{(Available in future)}\\
    This dimension is characterized by a bright white structure and by coloured elements in semi-transparency. Everything here is reduced to the essential.
\end{itemize}

\bigskip

Once a dimension is unlocked it is possible to travel back and forth from it.	

\newpage

\subsubsection{Interface}
The \textbf{interface} of the game consists of various screens. The main ones are:
\begin{itemize}
    \item \textbf{Title Screen}\\
    The entry screen with the \textbf{main menu} and the name of the game.
    
    \item \textbf{Map of the Current Dimension}\\
    In this screen is possible to \textbf{select the level} to play. There is a \textbf{path} that links all the levels of the current dimension and even \textbf{the character} is shown and placed on one of the levels.\\
    At the top of the display are shown the \textbf{name of the dimension} and the \textbf{number of the level} on which the character is.\\
    It is possible to open a little menu on this screen.
    
    \item \textbf{Level Screen}\\
    The levels have an \textbf{isometric view} and \textbf{no HUD} is present.\\
    It is possible to open a little menu inside each level.
    
    \item \textbf{Dimension Selection Screen}\\
    A selection screen of the \textbf{dimensions} present in the game. The unlocked ones are shown clearly, while the ones still locked are dark.
    
\end{itemize}

\subsubsection{Menu}
In this game there are \textbf{different menus} that allow to do different operations:
\begin{itemize}
    \item \textbf{Main Menu}
    \begin{itemize}
        \item \textit{New Game}: the player can start a new game.
        \item \textit{Continue}: the player can continue the adventure from the latest save.
        %\item \textit{Customize}: the player can customize the appearance of the character by changing the clothes or the hairstyle.
        \item \textit{Options}: several options to adjust the audio and video parameters.
        \item \textit{Credits}: information about the developers, the game and the credits.
        \item \textit{Exit Game}: the voice to return to the desktop.
    \end{itemize}
    
    \item \textbf{Map Dimension Menu}
    \begin{itemize}
        \item \textit{Resume}: close the menu.
        \item \textit{Change Dimension}: the \textbf{\textit{Dimension Selection Screen}} is displayed and the player can choose to travel to another dimension that has already been unlocked.
        \item \textit{Quit Game}: the voice to return to the \textbf{\textit{Title Screen}}.
    \end{itemize}
    
    \item \textbf{Level Menu}
    \begin{itemize}
        \item \textit{Resume}: close the menu and resume the current level.
        \item \textit{Map}: return to the \textbf{\textit{Map of the Current Dimension}} and cancel all the actions performed inside the level.
        \item \textit{Restart}: retry the current level by loading its initial state.
        \item \textit{Quit Game}: the voice to return to the \textbf{\textit{Title Screen}}.
    \end{itemize}
\end{itemize}

\newpage

\subsection{User Skills}
\textbf{\textit{DDoubled}} is a puzzle game in which every level can be solved thanks to the \textbf{skills of the player} in understanding all the \textbf{objects’ functionalities} and \textbf{their interactions} with the other elements and the environment itself.\\

The early levels of each dimension have the objective to \textbf{introduce} the player into the game’s world and to let they \textbf{become familiar} with the objects and mechanics required to complete the levels, which will be \textbf{intuitive} and suited for \textbf{every kind of player}.\\
In the following levels the player will have to use their \textbf{knowledge} of the objects in order to choose a \textbf{strategy} to solve all the present puzzles. The strategies \textbf{are different} for each level, so the game will always be fun.\\

\textbf{There isn't an amount time} to solve the levels, so the player can observe the entire scene and think about the \textbf{solution} of the puzzle \textbf{without haste}. If an object is used in the \textbf{wrong way} or if other \textbf{mistakes} are committed, the level can be \textbf{retried} and another strategy can be followed.\\

The \textbf{increasing skills} of the player will allow them to continue towards \textbf{more difficult} levels and the more complicated the puzzle, the more \textbf{satisfied} the player will be once it is solved.


\newpage

\subsection{Game Mechanics}
\label{gameMechanics}
Here is given the \textbf{detailed and complete list} of mechanics and items which characterize the game. The appearance of some objects \textbf{changes} according to the dimension, but for simplicity just one is shown here.

\begin{table}[H]
    \begin{tabular}{p{12cm} c}
        \begin{itemize}
        \item \textbf{Character}
            \begin{itemize}
                \item Walks
                \item Jumps
                \item Can be doubled
                \item Presses platforms
                \item Pulls levers
                \item Pushes objects such as boxes and cages
                \item Picks up objects such as spheres and keys
                \item Can break the glass
                %\item Collects the hidden items and can be customized with them
            \end{itemize}
        \end{itemize}
        
        &
        
        \raisebox{-14em}{\includegraphics[width=10em, keepaspectratio]{Images/Character.png}}
    \end{tabular}

    \begin{tabular}{p{12cm} c}
        \begin{itemize}
            \item \textbf{DDouble}
            \begin{itemize}
                \item Doubles the character that grabs it and disappears
                \item Doubles the objects carried by the character who touch it
            \end{itemize}
        \end{itemize}
        
        &
        
        \raisebox{-\totalheight}{\includegraphics[width=7em, keepaspectratio]{Images/DDouble3.jpg}}
    \end{tabular}
    
    \begin{tabular}{p{12cm} c}
        \begin{itemize}
            \item \textbf{PressablePlatform}
            \begin{itemize}
                \item Can be pressed by objects
                \item Activates effects on other objects while kept pressed
            \end{itemize}
        \end{itemize}
        
        &
        
        \raisebox{-8em}{\includegraphics[width=7em, keepaspectratio]{Images/Platform.jpg}}
    \end{tabular}
    
    \begin{tabular}{p{12cm} c}
        \begin{itemize}
            \item \textbf{Lever}
            \begin{itemize}
                \item Can be pulled
                \item Activates effects on other objects when pulled
                \item Once pulled can stay on position or return back after a certain amount of time, cancelling the effects activated
            \end{itemize}
        \end{itemize}
        
        &
        
        \raisebox{-9em}{\includegraphics[width=7em, keepaspectratio]{Images/Lever.jpg}}
    \end{tabular}
    
    \begin{tabular}{p{12cm} c}
        \begin{itemize}
            \item \textbf{WoodBox}
            \begin{itemize}
                \item Can be pushed
                \item Can be used to fill pits
                \item Can press platforms
                \item The character can jump directly on it
                \item Can break the glass
            \end{itemize}
        \end{itemize}
        
        &
        
        \raisebox{-9em}{\includegraphics[width=7em, keepaspectratio]{Images/WoodBox.jpg}}
    \end{tabular}
\end{table}
\begin{table}[H]
    \begin{tabular}{p{12cm} c}
        \begin{itemize}
            \item \textbf{IronBox}
            \begin{itemize}
                \item Can be pushed and decreases the character's speed
                \item Can be used to fill pits
                \item Can press platforms
                \item The character can jump directly on it
                \item Can break the glass
                \item Can be attracted and repelled by magnets
            \end{itemize}
        \end{itemize}
        
        &
        
        \raisebox{-11em}{\includegraphics[width=7em, keepaspectratio]{Images/IronBox.jpg}}
    \end{tabular}

    \begin{tabular}{p{12cm} c}
        \begin{itemize}
            \item \textbf{Cage}
            \begin{itemize}
                \item Can trap the character or other objects
                \item Can be pushed or can be too heavy to be moved by the character
                \item Too high to jump on it directly, but stay on it is possible
                \item Can be attracted and repelled by magnets
                \item Can break the glass
            \end{itemize}
        \end{itemize}
        
        &
        
        \raisebox{-10em}{\includegraphics[width=7em, keepaspectratio]{Images/Cage.jpg}}
    \end{tabular}
    
    \begin{tabular}{p{12cm} c}
        \begin{itemize}
            \item \textbf{IronSphere}
            \begin{itemize}
                \item Can be picked up and carried by the character
                \item Rolls
                \item Can press platforms
                \item Can break the glass
                \item Can be attracted and repelled by magnets
            \end{itemize}
        \end{itemize}
        
        &
        
        \raisebox{-10em}{\includegraphics[width=7em, keepaspectratio]{Images/IronSphere.jpg}}
    \end{tabular}
    
    \begin{tabular}{p{12cm} c}
        \begin{itemize}
            \item \textbf{LightSphere}
            \begin{itemize}
                \item Can be picked up and carried by the character
                \item Rolls
                \item Can press platforms
                \item Can break the glass
                \item Illuminates a small area around it
            \end{itemize}
        \end{itemize}
        
        &
        
        \raisebox{-10em}{\includegraphics[width=7em, keepaspectratio]{Images/LightSphere.png}}
    \end{tabular}
    
    \begin{tabular}{p{12cm} c}
        \begin{itemize}
            \item \textbf{Key}
            \begin{itemize}
                \item Can be picked up and carried by the character
                \item Can press platforms
                \item Can break the glass
                \item Opens doors
            \end{itemize}
        \end{itemize}
        
        &
        
        \raisebox{-8em}{\includegraphics[width=7em, keepaspectratio]{Images/Key.jpg}}
    \end{tabular}
\end{table}
\begin{table}[H]    
    \begin{tabular}{p{12cm} c}
        \begin{itemize}
            \item \textbf{Door}
            \begin{itemize}
                \item Usually closed, can be opened by keys, platforms or levers
                \item The character has to pass through it in order to complete the level
            \end{itemize}
        \end{itemize}
        
        &
        
        \raisebox{-8em}{\includegraphics[width=7em, keepaspectratio]{Images/Door.png}}
    \end{tabular}
    
    \begin{tabular}{p{12cm} c}
        \begin{itemize}
            \item \textbf{Magnet}
            \begin{itemize}
                \item Can attract or repel objects made by iron
                \item Can move even the character if it is carrying the \textit{IronSphere}
                \item The polarity can be switched by platforms or levers
                \item Cannot be moved
            \end{itemize}
        \end{itemize}
        
        &
        
        \raisebox{-8.5em}{\includegraphics[width=7em, keepaspectratio]{Images/Magnet.png}}
    \end{tabular}
    
    \begin{tabular}{p{12cm} c}
        \begin{itemize}
            \item \textbf{Portal}
            \begin{itemize}
                \item Can be an entry, an exit or both
                \item Used to teleport the character or other objects to another portal
                \item Can be activated by platforms or levers
            \end{itemize}
        \end{itemize}
        
        &
        
        \raisebox{-8em}{\includegraphics[width=7em, keepaspectratio]{Images/Portal.jpg}}
    \end{tabular}
    
    \begin{tabular}{p{12cm} c}
        \begin{itemize}
            \item \textbf{Glass}
            \begin{itemize}
                \item Can break under a specific force or weight
                \item Can be a path or an obstacle depending on the situation
            \end{itemize}
        \end{itemize}
        
        &
        
        \raisebox{-8em}{\includegraphics[width=7em, keepaspectratio]{Images/Glass.png}}
    \end{tabular}
    
    \begin{tabular}{p{12cm} c}
        \begin{itemize}
            \item \textbf{Conveyor Belt}
            \begin{itemize}
                \item Can move and launch the character or other objects on it
                \item Can be used as a slide
                \item The direction can be switched by platforms or levers
            \end{itemize}
        \end{itemize}
        
        &
        
        \raisebox{-9em}{\includegraphics[width=7em, keepaspectratio]{Images/ConveyorBelt.png}}
    \end{tabular}
    
    \begin{tabular}{p{12cm} c}
        \begin{itemize}
            \item \textbf{NumberedPlatform}
            \begin{itemize}
                \item Starts moving as soon as the displayed number of characters is on it
                \item Can be used as an elevator
                \item Can carry the characters on it to another point in the level
            \end{itemize}
        \end{itemize}
        
        &
        
        \raisebox{-8em}{\includegraphics[width=7em, keepaspectratio]{Images/NumberedPlatform.png}}
    \end{tabular}
    
    \begin{tabular}{p{12cm} c}
        \begin{itemize}
            \item \textbf{Hidden Item} \textit{(Available in future)}
            \begin{itemize}
                \item Located in some levels, usually hidden at sight
                \item Used to customize the character
                \item The full list can be checked in the customization screen
            \end{itemize}
        \end{itemize}
        
        &
        
        \raisebox{-8.5em}{\includegraphics[width=7em, keepaspectratio]{Images/HiddenItem.png}}
    \end{tabular}
\end{table}

\newpage

\subsection{Map and Levels}
Each dimension features a \textbf{map} developed horizontally which shows \textbf{all the levels} and the \textbf{areas} present. For example, the map of the \textbf{\textit{Pirate World}} starts with a \textbf{forest} and then it continues with a \textbf{beach}, the \textbf{sea} and a \textbf{pirate ship}.\\
The \textbf{appearance of the levels} recalls that of the area in which they are located, so that the player can \textbf{experience} the journey \textbf{together with} the main character.\\

The levels are delimited by \textbf{invisible walls} that prevent the character from \textbf{leaving the scene} and that \textbf{help} the player to \textbf{group the copies}, when needed.

%\subsection{Customization}
%\textbf{\textit{DDoubled}} gives the player the possibility to \textbf{personalize the character}. Both \textbf{hair and clothes} can be changed, but the different hairstyles and clothes have to be found \textbf{inside the levels} and usually they are \textbf{hidden} at sight.\\
%Some \textbf{special clothes} reflect the dimension in which they are found: for example, in the \textbf{\textit{Pirate World}} the \textbf{pirate hat} can be found.\\
%Furthermore, \textbf{at the end of the game} the \textbf{female character} will be unlocked.\\

%All these \textbf{hidden objects} can be worn by \textbf{both the male and the female} character and they \textbf{will change} according to the gender: for example, the hairstyle that makes a \textbf{ponytail} on the female character will do a hairdo with the \textbf{tuft} if used on the male character.

\subsection{Progression and Challenge}
In \textbf{\textit{DDoubled}} there will be \textbf{4 dimensions} composed of \textbf{10 levels} each.\\
When the player completes the 10th level of a dimension \textbf{for the first time} the next dimension is unlocked.
The difficulty of each one is set such that the early levels require \textbf{easy tasks} to understand the \textbf{new mechanics}, while the last levels present \textbf{more complex tasks} with \textbf{mixed game mechanics} which \textbf{challenge} the player.\\

In each dimension the \textbf{main goal} is to complete the \textbf{last level} and in order to reach it the player \textbf{is not forced} to complete \textbf{all the levels}, but only the ones that open a \textbf{path} from the first level to the 10th one.\\
This allows the player to \textbf{choose} another level if they can't find the \textbf{solution} of one.

\subsubsection{FlowChart}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.7]{Images/FlowChart.png}
    \caption{FlowChart}
    \label{fig:flowchart}
\end{figure}

\subsubsection{Player Experience}
The progression of the player through the levels is represented with a \textbf{graphical progression} in the map that makes the player experience \textbf{the same journey} of the character and the \textbf{increasing difficulty curve} inside each dimension stimulates their \textbf{ingenuity} and \textbf{reasoning}.\\
The possibility to \textbf{skip} some levels allows the player to continue to \textbf{enjoy} the game even if they are stuck at some point.

%The \textbf{customization} allows the player to dress the character according to \textbf{their tastes} or according to the dimension they are in and the \textbf{collection} of these \textbf{hidden items} increases both the \textbf{curiosity} of the player and the \textbf{life} of the game.

\subsection{Saving}
The \textbf{autosave} function is used and it occurs every time \textbf{a level is completed} and every time \textbf{another dimension is unlocked}.\\

Whenever the player selects to \textbf{continue} the game from the \textbf{main menu}, the \textbf{latest save} is loaded.

\subsection{Losing}
In this game there is \textbf{neither death nor violence}.\\
However there is no guarantee to pass some levels and in certain cases the player may be \textbf{stuck}. For example when an essential platform has been broken or when the character falls into a pit.\\
In these cases is always possible to \textbf{retry} the level or to \textbf{return} to the level selection's screen.

\newpage

\section{Art Style}
The \textbf{graphics} of the game is the union of \textbf{3D models with simple geometries}, enriched with \textbf{textures} for smaller details that do not require 3D modeling. The game has a \textbf{stylized appearance}, the \textbf{colors} are saturated and the \textbf{textures}, which are \textbf{digitally designed}, have a solid color at the base to which the details are superimposed. There are \textbf{no photorealistic textures} or deriving from photographs of real objects.

\subsection{Art Bible}
The \textbf{graphic style} of the game \textbf{is inspired} by games such as \href{https://en.wikipedia.org/wiki/Monument_Valley_(video_game)}{\textcolor{blue}{\textbf{\textit{\underline{Monument Valley}}}}} and, for some aspects, \href{https://en.wikipedia.org/wiki/The_Legend_of_Zelda:_Breath_of_the_Wild}{\textcolor{blue}{\textbf{\textit{\underline{The Legend of Zelda: Breath of the Wild}}}}}.\\

The \textbf{character} of the game is in \textbf{Chibi style}, a word belonging to the \textbf{Japanese slang} that describes something ``short". Indeed it has a big head and a tiny body.\\

Each \textbf{dimension} has its \textbf{own characteristics} and the elements of the gameplay as well as all the other decorative items in the scene \textbf{vary their appearance}, remaining \textbf{in line with the style} of the world to which they belong:
\begin{itemize}
    \item \textbf{\textit{Pirate World}} is characterized by many elements recalling \textbf{wood}, \textbf{water} and \textbf{sand}. Therefore the elements of the gameplay in this dimension have an appearance that recalls mainly the wood;
    \item \textbf{\textit{Ghost Kingdom}} is \textbf{dark} and \textbf{gloomy} and it is characterized by \textbf{cold and hard elements}, \textbf{fog} and \textbf{humidity}. The stone and the wet ground combined with a \textbf{faint illumination} are the most graphically present elements;
    \item \textbf{\textit{Neon City}} takes place at \textbf{night} and it features \textbf{bright}, \textbf{fluorescent} and \textbf{coloured} elements. As the name implies, there are many \textbf{neons};
    \item \textbf{\textit{Minimal Dimension}} consists of a \textbf{few decorative} elements, much more \textbf{geometric} than the ones in the other dimensions. It is characterized by a \textbf{bright white color} and it has an \textbf{anonymous} appearance.

\end{itemize}

\subsection{Concept Art Overview}
\begin{wrapfigure}[5]{l}{3in}
    \vspace{-1.2em}
    \centering
    \includegraphics[scale=0.2]{Images/Island.jpg}
    \caption{Example of the Concept Art}
    \label{fig:island}
\end{wrapfigure}

The figure \ref{fig:island} represents the \textbf{graphic style} of the game: \textbf{soft shadows}, \textbf{simple geometries} and \textbf{stylized textures}.\\
Each element of the game reflects this style, obviously \textbf{adapting} to the different dimensions.

\newpage

\section{Animations}
The animations are \textbf{simple}, matching the \textbf{graphic style} of the game. The \textbf{main animations} are those of the character, which is the only truly \textbf{dynamic element} of the game. The other \textbf{elements of the gameplay} are animated after the \textbf{interaction} with the character.\\
The \textbf{decorative elements} of the levels have \textbf{very simple animations}, such as the waves of the sea.\\

The animation of the \textbf{\textit{DDouble}} is important and it is realized with a \textbf{loop} of a sphere that \textbf{doubles} and then \textbf{returns} a single element, as shown in the following figures:

\begin{table}[H]    
    \begin{tabular}{c c c c c}
        \raisebox{0em}{\includegraphics[scale=0.1]{Images/DDouble1.jpg}}
        
        &
        
        \raisebox{0em}{\includegraphics[scale=0.1]{Images/DDouble2.jpg}}
        
        &
        
        \raisebox{0em}{\includegraphics[scale=0.1]{Images/DDouble3.jpg}}
        
        &
        
        \raisebox{0em}{\includegraphics[scale=0.1]{Images/DDouble2.jpg}}
        
        &
        
        \raisebox{0em}{\includegraphics[scale=0.1]{Images/DDouble1.jpg}}
    \end{tabular}
\end{table}

\subsection{Animation Types}
\begin{itemize}
    \item \textbf{Character}
    \begin{itemize}
        \item Walking
        \item Jumping
        %\item Doubling
        \item Pushing objects
        %\item Pulling levers
        \item Picking up objects
        \item Carrying objects
        \item Pushing objects while carrying an object
        \item Winning
        \item Scratching his head
    \end{itemize}
    
    \item \textbf{Gameplay Elements}
    \begin{itemize}
        \item \textit{DDouble}
        \item Level pulled
        \item Glass breaking
        \item Magnet's effect
        \item Moving of the conveyor belt
    \end{itemize}
    \item \textbf{Decorative Elements}
    \begin{itemize}
        \item Water waves
    \end{itemize}
\end{itemize}

\newpage

\section{Music and Sounds}
\subsection{Sound Styles}
The sound styles in \textbf{\textit{DDoubled}} are different. This is because \textbf{different dimensions} require \textbf{different sounds}.\\
The \textbf{common line} that unites the tracks is that all of them create a \textbf{fantastic and fair atmosphere} for the player which lasts for the \textbf{whole} journey of the character.\\

The musics are the \textbf{background} of the story and they \textbf{immerse} the player in the \textbf{best possible experience}. For this reason they must be \textbf{very expressive} and they mustn't be annoying for the player, naturally without losing the \textbf{essence} of the dimension that must be characterized.

\subsection{Sounds List}
\begin{itemize}
    \item Effects
    \begin{itemize}
        \item Footsteps
        \item Jump
        \item Sounds produced by bumps
        \item Glass break
        \item Sound produced by the conveyor belt
        \item Sound produced by the magnet
    \end{itemize}
    
    \item Feedback
    \begin{itemize}
        \item Open door
        \item Unlock door
        \item Press platform
        \item Pull lever
        \item Sound produced by the portal
    \end{itemize}
\end{itemize}

\subsection{Musics List}
\begin{itemize}
    \item Menu music: \textit{Memories} \footnote{\leftskip-1em \textcolor{blue}{\underline{\url{https://www.bensound.com/royalty-free-music/track/memories}}}}
    \item Pirate World: \textit{Brave Pirates} \footnote{\leftskip-1em \textcolor{blue}{\underline{\url{http://freemusicarchive.org/music/frievents_Orchestra/Pirate_Pop/Pirate_Pop_-_08_-_FriEvents_Orchestra_-_Brave_Pirates}}}}
    \item Ghost Kingdom: \textit{Cimitero Infestato} \footnote{\leftskip-1em \textcolor{blue}{\underline{\url{https://soundcloud.com/user-5431384/cimitero-infestato?in=user-5431384/sets/rpg-video-game-soundtracks-by-stefano-puricelli}}}} (Courtesy of  Stefano Puricelli)
    \item Neon City: \textit{Optimistic Bits} \footnote{\leftskip-1em \textcolor{blue}{\underline{\url{http://freemusicarchive.org/music/Phillip_Gross/Virtual_Instruments/Phillip_Gross_Virtual_Instruments_-_03_-_Optimistic_Bits}}}}
    \item Minimal Dimension: \textit{Deeply Dungeon} \footnote{\leftskip-1em \textcolor{blue}{\underline{\url{http://freemusicarchive.org/music/Lobo_Loco/Over_Midnight/Deeply_Dungeon_ID_957}}}}
    \item Credits: \textit{Arcade Paradise} \footnote{\leftskip-1em \textcolor{blue}{\underline{\url{http://freemusicarchive.org/music/Scott_Holmes/Music_For_Media/Scott_Holmes_-_01_-_Arcade_Paradise}}}}
\end{itemize}

\newpage

\section{Technical Spec}
\subsection{Development Platform and Tools}
\begin{itemize}
    \item \textbf{Unity}\\
    The game engine used for the creation of the levels and of the game itself.
    \item \textbf{Visual Studio}\\
    The IDE used to program all the scripts.
    \item \textbf{Blender}\\
    The 3D creation suite used to create all the models and animations.
    \item \textbf{Photoshop}\\
    The graphic editor used for creating the textures of the 3D models.
    \item \textbf{Audacity}\\
    The audio editing software used to cut the audio part and to fix the right speed time of the audio effects.
    
\end{itemize}

\subsection{Game Engine}
\textbf{Unity} is the game engine adopted for the development of the game and \textbf{C\#} is the language used.\\
Unity allows to \textbf{create} the levels, to \textbf{set} the objects' parameters and to \textbf{test} all the components and all the mechanics and interactions between all the elements of the game.

\subsubsection{Collision Detection}
The \textbf{collision detection} is all managed with \textbf{colliders} based on the shape of the objects: \textbf{capsule collider} for the character, \textbf{box collider} for the boxes, \textbf{sphere collider} for the spheres and so on.\\

\textbf{Trigger colliders} are used to detect when an object enters in the \textbf{action range} of another one, for example when an iron object enters in the action range of a magnet.

\subsection{Light}
A \textbf{static light} is used to illuminate the scene, but \textbf{its intensity and its color may vary} depending on the level.\\
In \textbf{\textit{Pirate World}} the light is bright, while the entire \textbf{\textit{Ghost Kingdom}} is characterized by a gloomy and dark atmosphere, so here the light is weak or even almost absent. In \textbf{\textit{Neon City}} multiple lights of different colours are used.\\

There are also some objects that have their \textbf{own light source}. It is the case of the \textbf{\textit{LightSphere}} and of the \textbf{lanterns} present in \textbf{\textit{Ghost Kingdom}} and of the \textbf{buildings} present in \textbf{\textit{Neon City}}.

\subsection{Rendering System}
\subsubsection{3D Rendering}
The game is developed in \textbf{3D isometric graphics}. The choice of a 3D graphics is mainly due to the \textbf{graphic rendering} to be achieved: \textbf{soft shadows} and \textbf{greater consistency} in the spatial arrangement of elements compared to 2D.

\subsubsection{Camera}
An \textbf{external camera with dynamic zoom} is used for the levels. This allows the player to have a \textbf{large vision} of the scene, so that it is possible to see the position of all the characters and \textbf{evaluate all the possible actions}.

\section{Platform and Audience}
This game is an \textbf{open source} project under \href{https://en.wikipedia.org/wiki/Creative_Commons_license}{\textcolor{blue}{\textbf{\underline{Creative Commons (CC) license}}}} and the \textbf{source code} is published on Gitlab: \textcolor{blue}{\underline{\url{https://gitlab.com/Drill/ddoubled}}}.\\

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.05]{Images/CC-Logo.png}
    \caption{Creative Commons License}
    \label{fig:CC}
\end{figure}

\textbf{\textit{DDoubled}} is developed for PC and distributed on \textbf{Windows} and \textbf{Linux} and the complete absence of violence and sex means that it can be played by \textbf{everyone}.

\section{Similar Games}
The concept of using \textbf{multiple versions} of themselves in order to \textbf{solve puzzles} has been already used in games such as \href{https://en.wikipedia.org/wiki/Echoshift}{\textcolor{blue}{\textbf{\textit{\underline{Echoshift}}}}} and \href{https://en.wikipedia.org/wiki/Ratchet_\%26_Clank_Future:_A_Crack_in_Time}{\textcolor{blue}{\textbf{\textit{\underline{Ratchet \& Clank: A Crack in Time}}}}} (during Clank's gameplay), but both allow the player to \textbf{record a sequence of actions} that will then be repeated by \textbf{holograms}, while the main character can move freely, \textbf{independently} by them.\\

In \textbf{\textit{DDoubled}} there isn’t this distinction between past and present actions: everything happens \textbf{at the same time} and all the copies are controlled by \textbf{the same commands}.\\
This is what happens in \href{https://en.wikipedia.org/wiki/Super_Mario_3D_World}{\textcolor{blue}{\textbf{\textit{\underline{Super Mario 3D World}}}}} when you grab the power up named \href{https://www.mariowiki.com/Double_Cherry}{\textcolor{blue}{\textbf{\textit{\underline{Double Cherry}}}}}, but here the copies \textbf{aren’t used to solve puzzles} and basically they are just an extra opportunity to hit enemies.

\newpage

\section{Writing}
\subsection{Script}
Short sentences introduce the player to the game and no other scripts are present.

\subsection{Game Tutorial and Manual}
\subsubsection{System Requirements}
\begin{itemize}
    \item Operating Systems: Windows 7/8/8.1/10, Linux
    \item Controller is optional
\end{itemize}

\subsubsection{Installation Instructions}
The game doesn't require installation and the procedure to play it is very easy:
\begin{enumerate}
    \item Download the game;
    \item Open the executable file (see figure \ref{fig:gameIcon});
    \item Set your graphic options;
    \item Enjoy!
\end{enumerate}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.1]{Images/DDoubled-Icon.png}
    \caption{Game Icon}
    \label{fig:gameIcon}
\end{figure}

\newpage

\subsubsection{How To Play}
In \textbf{\textit{DDoubled}} is possible to play with both \textbf{keyboard} and \textbf{controller}.\\

\begin{itemize}
    \item \textbf{Keyboard}\\
    The \textbf{arrow keys} or the \textbf{WASD keys} (see figure \ref{fig:keyboard}) are used to control the \textbf{character's movements}. With these commands the character changes its rotation and starts walking in that direction.\\
    Another important control is the \textbf{jump}, which can be executed using the \textbf{space bar}.\\
    To pull the \textbf{levers} the \textbf{E key} is used.\\
    To \textbf{pick up} some objects press the \textbf{F key} once and press it \textbf{again} in order to \textbf{put down} the object held.\\
    The \textbf{Esc key} opens the menu.\\
    
\begin{figure}[H]
    \centering
    \includegraphics[scale=0.35]{Images/Keyboard.png}
    \caption{Keyboard Controls}
    \label{fig:keyboard}
\end{figure}

    \item \textbf{Controller}\\
    There are two possible ways to control the \textbf{character's movements}: using the \textbf{left-pad} or using the \textbf{D-pad} (see figure \ref{fig:controller}).\\
    The \textbf{A button} (the green one in figure) is used to \textbf{jump}.\\
    The \textbf{X button} (the red one in figure) is used to \textbf{pick up} some objects and to \textbf{put down} them.\\
    The \textbf{B button} (the blue one in figure) is used to pull \textbf{levers}.\\
    The \textbf{Start button} opens the menu.
    
\begin{figure}[H]
    \centering
    \includegraphics[scale=0.25]{Images/Controller.jpg}
    \caption{Controller Controls}
    \label{fig:controller}
\end{figure}
    
\end{itemize}

\newpage

This game features a lot of \textbf{different objects} that the player has to use to solve the puzzles. Different object means \textbf{different interaction}:
\begin{itemize}
    \item The \textbf{boxes} can be pushed
    \item The \textbf{keys} and the \textbf{spheres} can be carried
    \item The \textbf{portals} are used to teleport to another portal
    \item The \textbf{levers} can be pulled
    \item The \textbf{pressable platform} can be pressed
    \item The \textbf{conveyor belts} can be exploited to launch objects or the character itself
\end{itemize}

\bigskip

\textit{The complete list of the mechanics is reported in the \textbf{\nameref{gameMechanics}} section (\textcolor{blue}{3.3}).}
    

\section{Walkthrough}
Here will be reported the \textbf{solutions} of the levels. For the \textbf{enjoyment} of the game, reading this part \large{\textbf{is only recommended as a last resort}}.

\subsection{Levels' Solutions}
Coming soon...

\end{document}