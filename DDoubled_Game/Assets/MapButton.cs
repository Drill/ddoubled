﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapButton : MonoBehaviour
{
    public int number;

    private void Awake()
    {

        StoredDataScript data = SaveSystem.LoadData();
        if (data != null)
        {
            if (data.Dimension[number])
            this.gameObject.GetComponent<Image>().color = new Color(255, 255, 255, 1f);
        }
    }
}
