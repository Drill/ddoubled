﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeableObjectState : MonoBehaviour {

    public bool isTaken = false;
    public bool isGravity = true;
    private Rigidbody obj;
    private bool findPlayer;
    private bool detect;
    private Animator anim;

    public void Awake()
    {
        anim = GetComponent<Animator>();
        findPlayer = false;
        detect = false;
        obj = GetComponent<Rigidbody>();   
    }

    public void SetTaken(bool flag)
    {
        isTaken = flag;
    }

    public bool GetTaken()
    {
        return isTaken;
    }

    public void SetGravity(bool flag)
    {
        isGravity = flag;
    }

    public bool GetGravity()
    {
        return isGravity;
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            findPlayer = true;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            anim.SetBool("Taken", true);
    }

    public void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Player"))
            anim.SetBool("Taken", false);
    }
    public void OnCollisionExit(Collision collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            findPlayer = false;
        }
    }

    public void Init()
    {
        obj.useGravity = isGravity;
        if (isGravity)
        {
            obj.constraints = RigidbodyConstraints.FreezeRotation;
            obj.isKinematic = false;
            obj.detectCollisions = true;
        }
        else
        {
            obj.constraints = RigidbodyConstraints.FreezeAll;
            obj.isKinematic = true;
            obj.detectCollisions = false;
        } 
    }

    public bool GetCollision()
    {
        return findPlayer;
    }
    public void CloneState(TakeableObjectState state)
    {
        SetTaken(state.GetTaken());
        SetGravity(state.GetGravity());
    }

    public bool GetDetect()
    {
        return detect;
    }

    public void SetDetect(bool flag)
    {
        detect = flag;
    }
}
