﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slide_Script : MonoBehaviour
{
    
    public Transform endpoint;
    //public GameObject block_point;
    
    public float speed; // 15 is a good value for a slide
    private bool onmovement;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
       
    }


    // the object that is on this object is transported forward
    /*void OnTriggerStay(Collider other)
    {
        block_point.SetActive(false);
        onmovement = true;
        if (onmovement)
        other.transform.position = Vector3.MoveTowards(other.transform.position, endpoint.position, speed * Time.deltaTime);
    }*/

    void OnTriggerStay(Collider other)
    {
        if (!other.isTrigger && other.gameObject.transform.parent == null)
        {
            //block_point.SetActive(false);
            onmovement = true;
            if (onmovement)
                other.transform.position = Vector3.MoveTowards(other.transform.position, endpoint.position, speed * Time.deltaTime);
           
        }
    }

   /* void OnTriggerExit(Collider other)
    {
      
        onmovement = false;
        block_point.SetActive(true);
        
    }*/


}
