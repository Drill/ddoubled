﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Little GameManager that i've created to test my code, but that if we want to can be completed with other methods
public class GameManager : MonoBehaviour {

    public static GameManager instance { get; private set; }
    public GameObject player;
    public bool[] savedLevel;
    public bool fullScreen;
    public float mainSoundLevel;
    public float effectSoundLevel;
    public float musicSoundLevel;
    public bool isometricMotionCorrection;
    public string resolution;
    public int quality;
    public bool alreadyStartAgame;
    public bool[] Dimension;


    private GameObject playerInstantiated;
    private CameraControl camera;
    private MusicAudioManager musicAudioManager;
    private bool takenHiddenObject;
    
	// Use of Singleton Pattern
	private void Awake () {

        if (instance == null)
        {
            instance = this;
            camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraControl>();
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
        musicAudioManager = GameObject.FindGameObjectWithTag("MusicManager").GetComponent<MusicAudioManager>();
        LoadGame();
	}

    internal void StopMusic()
    {
        musicAudioManager.stopMusic();
    }

    private void OnEnable()
    {
        EventManager.StartListening("NextLevel", NextLevel);
        EventManager.StartListening("HiddenObjectTaken", HiddenObjectTaken);
    }

    private void HiddenObjectTaken()
    {
        takenHiddenObject = true;
    }

    private void NextLevel()
    {
        if (!takenHiddenObject)
            Invoke("ChangeScene", 4f);
        else
            ShowReward();
    }

    private void ShowReward()
    {
        Debug.Log("Hey!! You've taken an hidden object");
        Invoke("ChangeScene", 4f);
    }

    private void ChangeScene()
    {
        int currentLevel = SceneManager.GetActiveScene().buildIndex;
        int newLevel = currentLevel + 1;
        takenHiddenObject = false;
        //MusicControl(newLevel);
        switch (currentLevel)
        {
            case 3:
                SceneManager.LoadScene(6);
                MusicControl(6);
                break;

            case 21:
                if (!this.savedLevel[23])
                {
                    this.savedLevel[23] = true;
                    Dimension[1] = true;
                    SaveSystem.SaveData(instance);
                }

                SceneManager.LoadScene(4);
                MusicControl(2);
                break;
            case 32:
                if (!this.savedLevel[34])
                {
                    this.savedLevel[34] = true;
                    Dimension[2] = true;
                    SaveSystem.SaveData(instance);
                }

                SceneManager.LoadScene(4);
                MusicControl(2);
                break;
            case 43:
                SceneManager.LoadScene(45);
                MusicControl(45);
                break;
            case 44:
                SceneManager.LoadScene(5);
                MusicControl(5);
                break;

            default:
                LevelToUnlock(currentLevel);
                SceneManager.LoadScene(newLevel);
                break;
        }

      
        SaveSystem.SaveData(instance);
    }

    private void LevelToUnlock(int currentLevel)
    {

        switch (currentLevel)
        {
            case 13:
                this.savedLevel[14] = true;
                this.savedLevel[15] = true;
                break;
            case 14:
                this.savedLevel[16] = true;
                break;
            case 18:
                this.savedLevel[19] = true;
                this.savedLevel[20] = true;
                break;
            case 19:
                this.savedLevel[21] = true;
                break;
            case 24:
                this.savedLevel[25] = true;
                this.savedLevel[26] = true;
                break;
            case 25:
                this.savedLevel[27] = true;
                break;
            case 26:
                this.savedLevel[28] = true;
                break;
            case 39:
                this.savedLevel[40] = true;
                this.savedLevel[41] = true;
                break;
            case 40:
                this.savedLevel[42] = true;
                break;
            case 41:
                this.savedLevel[42] = true;
                this.savedLevel[43] = true;
                break;
            default:
                this.savedLevel[(currentLevel+1)] = true;
                break;

        }
    }

    public void MusicControl(int newLevel)
    {
        //Number that are not real, will be change after at the end
        if (newLevel == 45 || newLevel == 44)
            musicAudioManager.ChangeWorldMusic(5);
        if (newLevel >= 5 && newLevel <= 21)
            musicAudioManager.ChangeWorldMusic(1);
        else if (newLevel >= 22 && newLevel <= 32)
            musicAudioManager.ChangeWorldMusic(2);
        else if (newLevel >= 33 && newLevel <= 43)
            musicAudioManager.ChangeWorldMusic(3);
        else
            musicAudioManager.ChangeWorldMusic(0);
            
    }

    public void LoadGame()
    {
        StoredDataScript data = SaveSystem.LoadData();
        if(data != null)
        { 
            this.mainSoundLevel = data.masterSoundLevel;
            this.musicSoundLevel = data.musicSoundLevel;
            this.effectSoundLevel = data.effectSoundLevel;
            this.fullScreen = data.fullScreen;
            this.savedLevel = data.level;
            this.Dimension = data.Dimension;
            this.quality = data.screenQuality;
            this.alreadyStartAgame = data.alreadyStartAgame;
            this.resolution = data.resolution;
            this.isometricMotionCorrection = data.isometricMotionCorrection;
           

        }
        else
        {
            this.mainSoundLevel = 0;
            this.musicSoundLevel = 0;
            this.effectSoundLevel = 0;
            this.fullScreen = true;
            this.savedLevel = new bool[SceneManager.sceneCountInBuildSettings];

            this.Dimension = new bool[4];
            this.Dimension[0] = true;
            this.Dimension[1] = false;
            this.Dimension[2] = false;
            this.Dimension[3] = false;

            //Change these values at the end of the game
            // it resets all the values for a new game and some scene should be always playable
            for (int i = 0; i < this.savedLevel.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        this.savedLevel[i] = true;
                        break;
                    case 1:
                        this.savedLevel[i] = true;
                        break;
                    case 2:
                        this.savedLevel[i] = true;
                        break;
                    case 3:
                        this.savedLevel[i] = true;
                        break;
                    case 4:
                        this.savedLevel[i] = true;
                        break;
                    case 5:
                        this.savedLevel[i] = true;
                        break;
                    case 6:
                        this.savedLevel[i] = true;
                        break;
                    case 12:
                        this.savedLevel[i] = true;
                        break;
                    case 22:
                        this.savedLevel[i] = true;
                        break;
                    case 33:
                        this.savedLevel[i] = true;
                        break;
                    default:
                        this.savedLevel[i] = false;
                        break;
                }
            }
            this.quality = 0;
            this.isometricMotionCorrection = false;
            this.alreadyStartAgame = false;
        }
    }

    internal bool CheckUnlockedDimension(int indexOfDimensionToLoad)
    {

        if (indexOfDimensionToLoad == 22)
        {
            if (Dimension[1])
            {
                return true;
            }
            else
                return false;
        }
        else if (indexOfDimensionToLoad == 33)
        {
            if (Dimension[2])
            {
                return true;
            }
            else
                return false;
        }
        else if (indexOfDimensionToLoad == 5)
        {
            return true;
        }
        else
            return false;

    }
}
