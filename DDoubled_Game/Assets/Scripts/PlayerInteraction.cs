﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour {

    public RuntimeAnimatorController newController;

    private Animator anim;
    public bool sphereDetect;
    private bool leverDetect;
    private bool boxDetect;
    private bool keyDetect;
    private CameraControl camera;
    public GameObject takeableObject;
    private PlayerState ownState;
    private Vector3 hand = new Vector3(0.02f, 0.643f, .4f);

    private readonly object _syncRoot = new object();   // object used in the synchronized method for the interaction of the character with the objects

    private void Awake()
    {
        //If we have a static camera we must comment all of parts that call camera
        camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraControl>();
        anim = GetComponent<Animator>();
        ownState = GetComponent<PlayerState>();
    }

    public void OnEnable()
    {
        EventManager.StartListening("NextLevel", NextLevel);
        EventManager.StartListening("StartTheGame", StartTheGame);
    }

    private void Update()
    {
        if ( sphereDetect && Input.GetButtonDown("LongInteraction") && ownState.GetIsHandFree())
        {
            PickUp();
            ownState.SetHandFree(false);
            Debug.Log("Hey, i take a sphere");
        }else if (keyDetect && Input.GetButtonDown("LongInteraction") && ownState.GetIsHandFree())
        {
            PickUp();
            ownState.SetHandFree(false);
            Debug.Log("Hey, i take a key");
        }
        else if (leverDetect && Input.GetButtonDown("ImmediateInteraction") && ownState.GetIsHandFree())
        {
            Debug.Log("SendMessage");
            //Decide a common name for the event
            EventManager.TriggerEvent("MechanismActivation");
        }
        else if (Input.GetButtonDown("LongInteraction") && !ownState.GetIsHandFree())
        {
            anim.SetBool("PickUp", false);
            //anim.SetTrigger("DropDown");
            DropDown();
            Invoke("ResetAnimation", .3f);
            ownState.SetHandFree(true);
        }
    }

    private void OnCollisionEnter(Collision other)
    {

        

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("DDouble") && !other.GetComponent<DDouble_Script>().IsUsed())
        {
            other.GetComponent<DDouble_Script>().UseDDouble();
            Destroy(other.gameObject);

            // lock the player in order to run the animation of the doubling
            gameObject.GetComponent<PlayerMovements>().enabled = false;

            GameObject mom;
            mom = Instantiate(gameObject, new Vector3(transform.position.x + 1.5f, transform.position.y, transform.position.z), transform.rotation);
            camera.targets.Add(mom.transform);


            PlayerState newPlayerState = mom.GetComponent<PlayerState>();
            newPlayerState.CloneState(ownState);
            if (!ownState.GetIsHandFree())
            {
                TakeableObjectState childsObject = mom.GetComponentInChildren<TakeableObjectState>();
                childsObject.CloneState(takeableObject.GetComponent<TakeableObjectState>());
                childsObject.Init();
                mom.GetComponent<PlayerInteraction>().setTakeableObject(mom.GetComponentInChildren<TakeableObjectState>().gameObject);
                
                mom.GetComponent<Animator>().runtimeAnimatorController = newController;

            }

            // wait 0.5 seconds and then enable the movements again
            Wait(0.5f, () => {
                gameObject.GetComponent<PlayerMovements>().enabled = true;
                mom.GetComponent<PlayerMovements>().enabled = true;
            });
        }
        else if (other.CompareTag("Lever"))
        {
            //Thing how to do the animation
            leverDetect = true;
            Debug.Log("Lever In" + leverDetect);
        }else if (other.CompareTag("Sphere") && ownState.GetIsHandFree())
        {
            if (!other.GetComponent<TakeableObjectState>().GetDetect())
            {
                other.GetComponent<TakeableObjectState>().SetDetect(true);
                takeableObject = other.gameObject;
                Debug.Log(takeableObject);
                sphereDetect = true;
                Debug.Log("SphereDetect");
            }  
        }
        else if (other.CompareTag("Key") && ownState.GetIsHandFree())
        {
            if (!other.GetComponent<TakeableObjectState>().GetDetect())
            {
                other.GetComponent<TakeableObjectState>().SetDetect(true);
                takeableObject = other.gameObject;
                Debug.Log(takeableObject);
                keyDetect = true;
                Debug.Log("KeyDetect");
            }
        }
    }

    private void OnCollisionExit(Collision other)
    {
        
        if (other.collider.CompareTag("PressablePlatform"))
        {
            Debug.Log("PressablePlatformOut");
        }
    }

    

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Lever"))
        {
            leverDetect = false;
            Debug.Log("LeverOut");
        }
        else if (other.CompareTag("Sphere"))
        {
            sphereDetect = false;
            other.GetComponent<TakeableObjectState>().SetDetect(false);
            Debug.Log("SphereOut");
        }
        else if (other.CompareTag("Key"))
        {
            keyDetect = false;
            other.GetComponent<TakeableObjectState>().SetDetect(false);
            Debug.Log("KeyOut");
        }
    }
    private void PickUp()
    {
        if (takeableObject != null)
        {
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("CharacterIdle"))
            {
                gameObject.GetComponent<PlayerMovements>().enabled = false;
                anim.SetBool("PickUp", true);
                //anim.SetTrigger("PickUp");

                Invoke("PickUpReallyAnObject", .5f);
                Wait(0.5f, () =>
                {
                    gameObject.GetComponent<PlayerMovements>().enabled = true;
                });
                }
        }else
        {
            keyDetect = false;
            sphereDetect = false;
        }
    }

    private void PickUpReallyAnObject()
    {
        try
        {
            TakeableObjectState state = takeableObject.GetComponent<TakeableObjectState>();
            state.SetTaken(true);
            state.SetGravity(false);
            state.Init();
            takeableObject.transform.parent = transform;
            takeableObject.transform.localPosition = hand;
            takeableObject.transform.localRotation = Quaternion.AngleAxis(90, new Vector3(-1, 0, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
        } catch (NullReferenceException e1) {
            Debug.Log("Action button released too early. Repeat the action.");
        } catch(MissingReferenceException e2) {
            Debug.Log("A key has been destroyed. You can't pick up it.");
        }
    }

    private void DropDown()
    {
        try
        {
            if (takeableObject != null)
            {
                TakeableObjectState state = takeableObject.GetComponent<TakeableObjectState>();
            state.SetTaken(false);
            state.SetGravity(true);
            state.Init();
            takeableObject.transform.parent = null;
            takeableObject = null;
            }
            else
            {
                keyDetect = false;
                sphereDetect = false;
            }
        }
        catch (NullReferenceException)
        {
            Debug.Log("What dropdown if you haven't an object??");
        }
    }

    public void ResetAnimation()
    {
        anim.SetBool("IsPushingWithObject", false);
        anim.SetBool("IsJumping", false);
        anim.SetBool("IsWalkingWithObject", false);
    }

    public void NextLevel()
    {
        gameObject.GetComponent<PlayerMovements>().enabled = false;
        EventManager.StopListening("NextLevel", NextLevel);
        anim.SetTrigger("EndLevel");
    }

    public void StartTheGame()
    {
        Debug.Log("EnableMovement");
        gameObject.GetComponent<PlayerMovements>().enabled = true;
        EventManager.StopListening("StartTheGame", StartTheGame);
    }

    // create a coroutine in order to lock the character when it doubles
    public void Wait(float seconds, Action action)
    {
        StartCoroutine(_wait(seconds, action));
    }
    // the coroutine to be executed
    IEnumerator _wait(float time, Action callback)
    {
        yield return new WaitForSeconds(time);
        callback();
    }

    public void setTakeableObject(GameObject taken)
    {
        takeableObject = taken;
    }

}
