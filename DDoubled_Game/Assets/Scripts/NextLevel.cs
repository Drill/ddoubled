﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevel : MonoBehaviour {
    public static bool changingScene;
    // Use this for initialization
    void Awake() {
        changingScene = false;
    }

    // Update is called once per frame
    void Update() {

    }

    public void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            changingScene = true;
            EventManager.TriggerEvent("NextLevel");
        }
    }
}
