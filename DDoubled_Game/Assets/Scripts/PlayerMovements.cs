﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovements : MonoBehaviour {

    //Parameters to manage the player exect sounds
    [Header("Player sound source end clips")]
    public AudioSource playerSoundSource;
    public AudioClip footstepSound;
    public AudioClip stomachSound;
    public AudioClip jumpSound;

    //Player' speed
    [Header("Player' speeds")]
    public float fowardSpeed = 6f;

    //Fixed time that the player must attend to do a static animation and that doesn't change
    [Header("Wating time to activate some static animation")]
    public float waitingTime = 5f;

    [Header("Height of jump & length")]
    public float jumpHigh = 330f;

    private Animator anim;
    private float speed;
    private Rigidbody playerRigidbody;
    private CapsuleCollider playerCollider;
    private Vector3 movement;
    //Timer that is decreamented each time that the player is in static position
    private float countdown;
    //Different bools use to implement the player movements
    private bool isWalking;
    private bool isPushing;
    private float landingTime; // time to wait in order to do another jump. This avoid a quick double jump
    private bool isGrounded;
    private CameraControl camera;
    private GameObject takenObject;
    private Vector3 hand = new Vector3(0.02f, 0.643f, .4f);
    private Vector3 armpit = new Vector3(-0.3f,0.5f,0.15f);
    private Vector3 foward;
    private Vector3 right;
    public bool isometricCorrection;

    public LayerMask groundLayer;   // the layer on which the character can jump

    private void Awake()
    {
        UnityEngine.Random.InitState(System.DateTime.Now.Millisecond);
        playerRigidbody = GetComponent<Rigidbody>();
        playerCollider = GetComponent<CapsuleCollider>();
        camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraControl>();
        anim = GetComponent<Animator>();
        speed = fowardSpeed;
        isPushing = false;
        isWalking = false;
        ResetTime();
        playerRigidbody.freezeRotation = true;
        landingTime = 0f;  // the player can jump
        groundLayer = LayerMask.GetMask("Default") | LayerMask.GetMask("Iron");
        if (GameManager.instance != null)
            isometricCorrection = GameManager.instance.isometricMotionCorrection;
        else
            isometricCorrection = true;

        foward = camera.transform.forward;
        foward.y = 0f;
        foward = Vector3.Normalize(foward);
        right = Quaternion.Euler(new Vector3(0f, 90f, 0f)) * foward;
    }

    private void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        isGrounded = IsGrounded();  // check the value at each frame, not only onCollisionEnter
        
        if(landingTime <= 0)
            anim.SetBool("IsJumping", false);

        if (isGrounded && landingTime <=0 && 
            Input.GetAxisRaw("Jump").Equals(1))
        {
            ResetStaticAnimation();
            Jumping();
        }else if (h == 0 && v == 0 && isGrounded)
        {
            if(isPushing || isWalking)
                SetStatic();
            else
            {
                if ((anim.GetCurrentAnimatorStateInfo(0).IsName("CharacterIdle")))
                {
                    countdown -= Time.deltaTime;
                    if (countdown <= 0)
                        //StaticAnimation(UnityEngine.Random.Range(0, 10));
                        StaticAnimation(1);
                }
            }
        }
        else
        {
            ResetTime();
            if (gameObject.GetComponent<PlayerState>().GetIsHandFree())
            {
                CheckDoublePlayer();
                takenObject = null;
                WalkingOrPushingWithFreeHand();
                Move(h, v);
                Turning();
            }else
                try
                {
                    if (!gameObject.GetComponentInChildren<TakeableObjectState>().GetCollision())
                    {
                        WalkingOrPushingWithoutFreeHand();
                        Move(h, v);
                        Turning();
                        SetObjectPosition();
                    }
                }
                catch (NullReferenceException)
                {
                    gameObject.GetComponent<PlayerInteraction>().setTakeableObject(null);
                    gameObject.GetComponent<PlayerState>().SetHandFree(true);
                }
        }
        if (landingTime > 0)
            landingTime -= Time.deltaTime;
    }

        private void CheckDoublePlayer()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("CharacterIdleWithObject"))
        {
            anim.SetBool("PickUp", false);
            //anim.SetTrigger("DropDown");
        }
    }

    private void ResetTime()
    {
        countdown = UnityEngine.Random.Range(5, 50);
    }

    private void SetObjectPosition()
    {
        if (takenObject == null)
        {
            foreach (Transform child in transform)
            {
                if (child.tag.Equals("Key") || child.tag.Equals("Sphere"))
                    takenObject = child.gameObject;
            }
        }
        else
        {
            if (isPushing && !takenObject.transform.position.Equals(armpit))
            {
                takenObject.transform.localPosition = armpit;
                takenObject.transform.localRotation = Quaternion.AngleAxis(90, new Vector3(1, 0, 0))*Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) ;
            }
            else if (!takenObject.transform.position.Equals(hand))
            {
                takenObject.transform.localRotation = Quaternion.AngleAxis(90, new Vector3(-1, 0, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                takenObject.transform.localPosition = hand;
            }

        }
    }

    private void WalkingOrPushingWithFreeHand()
    {
        ResetStaticAnimation();
        if (isGrounded)
        {
            if (isPushing)
            {
                isWalking = false;
                anim.SetBool("IsPushing", true);
                if (!playerSoundSource.isPlaying)
                    playerSoundSource.PlayOneShot(footstepSound);
            }
            else
            {
                isWalking = true;
                anim.SetBool("IsWalking", true);
                if (!playerSoundSource.isPlaying)
                    playerSoundSource.PlayOneShot(footstepSound);
            }
        }
    }

    private void ResetStaticAnimation()
    {
        if (anim.GetBool("StaticAnimation")) { 
            anim.SetBool("StaticAnimation", false);
            playerSoundSource.Stop();
            playerSoundSource.transform.position = new Vector3(0f, 0f, 0f);
        }
    }

    private void WalkingOrPushingWithoutFreeHand()
    {
        if (isGrounded)
        {
            if (isPushing)
            {
               // isWalking = false;
                anim.SetBool("IsWalkingWithObject", false);
                anim.SetBool("IsPushingWithObject", true);
                if (!playerSoundSource.isPlaying)
                    playerSoundSource.PlayOneShot(footstepSound);
            }
            else
            {
                isWalking = true;
                anim.SetBool("IsWalkingWithObject", true);
                if (!playerSoundSource.isPlaying)
                    playerSoundSource.PlayOneShot(footstepSound);
            }
        }
    }

    private void Turning()
    {
        if(movement != Vector3.zero)
            transform.rotation = Quaternion.LookRotation(movement);
    }

    private void Move(float h, float v)
    {
        if (isometricCorrection)
        {
            Vector3 direction = new Vector3(h, 0f, v);

            Vector3 rightMovement = right  * h;
            Vector3 upMovement = foward * v;

            movement = Vector3.Normalize(rightMovement + upMovement);
            movement = movement.normalized * speed * Time.deltaTime;
            playerRigidbody.MovePosition(transform.position + movement);

            if (movement != Vector3.zero)
                transform.forward = movement;

        }
        else
        {
            movement.Set(h, 0f, v);
            movement = movement.normalized * speed * Time.deltaTime;
            playerRigidbody.MovePosition(transform.position + movement);
        }
    }

    private void StaticAnimation(int animation)
    {
        /*animation = animation * animation;
        if(animation > 0 && animation < 20)
        {
            playerSoundSource.transform.position = new Vector3(0f, 1f, 0f);
            playerSoundSource.clip = stomachSound;
            playerSoundSource.Play();
        }
        else if(animation >= 20 && animation < 60)
        {
            Invoke("SetStatic", 1.8f);
            anim.SetBool("StaticAnimation", true);
        }
        else if(animation >= 60 && animation < 100)
        {
            Invoke("SetStatic", 2f);
            anim.SetBool("StaticAnimation1", true);
        }*/
        Invoke("SetStatic", 1.8f);
        anim.SetBool("StaticAnimation", true);
        playerSoundSource.transform.position = new Vector3(0f, 1f, 0f);
        playerSoundSource.clip = stomachSound;
        playerSoundSource.Play();
    }

    private void SetStatic()
    {
        if (anim.GetBool("IsWalking"))
        {
            isWalking = false;
            anim.SetBool("IsWalking", false);
        }
        else if (anim.GetBool("IsPushing"))
            anim.SetBool("IsPushing", false);
        else if (anim.GetBool("IsWalkingWithObject"))
        {
            isWalking = false;
            anim.SetBool("IsWalkingWithObject", false);
        }
        else if (anim.GetBool("IsPushingWithObject"))
            anim.SetBool("IsPushingWithObject", false);
        else if (anim.GetBool("StaticAnimation"))
            anim.SetBool("StaticAnimation", false);

        speed = fowardSpeed;
        countdown = waitingTime;
        playerSoundSource.Stop();
        playerSoundSource.transform.position = new Vector3(0f, 0f, 0f);
    }

    private void Jumping()
    {
        anim.SetBool("IsJumping", true);
        landingTime = .6f;
        ResetTime();
        playerRigidbody.AddForce(0f, jumpHigh, 0f);
        playerSoundSource.Stop();
        if (playerSoundSource.clip != jumpSound)
            playerSoundSource.PlayOneShot(jumpSound);
    }

    private bool IsGrounded()
    {
        // return true if the collision with the specified layer happens vertically
        return Physics.CheckCapsule(playerCollider.bounds.center, new Vector3(playerCollider.bounds.center.x, playerCollider.bounds.min.y, playerCollider.bounds.center.z),
                                    playerCollider.radius * 0.9f, groundLayer);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (isGrounded)
            anim.SetBool("IsJumping", false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("WoodBox") || other.CompareTag("MetalBox") || other.CompareTag("Cage"))
        {
            isPushing = true;
            anim.SetBool("IsWalking", false);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("WoodBox") || other.CompareTag("MetalBox") || other.CompareTag("Cage"))
        {
            isPushing = false;
            anim.SetBool("IsPushing", false);
            anim.SetBool("IsPushingWithObject", false);
        }
    }

}
