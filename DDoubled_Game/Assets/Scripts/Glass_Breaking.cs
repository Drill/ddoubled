﻿using UnityEngine;
using System.Collections;

public class Glass_Breaking : MonoBehaviour 
{
	public Transform brokenObject;
	public float mass_accepted, magnitude_accepted, radius, power, upwards;
    private float total_mass=0;
    private int objectTaken = 0;
    bool onecollision=false;

	void OnCollisionEnter(Collision collision)
	{
        if (!collision.collider.isTrigger && !collision.collider.CompareTag("Untagged"))
        {
            
            total_mass += collision.rigidbody.mass;
            if (collision.collider.CompareTag("Player"))
            {

                if (!collision.gameObject.GetComponent<PlayerState>().GetIsHandFree())
                {
                    objectTaken += 1;
                    total_mass += collision.rigidbody.mass;
                }
            }else if (objectTaken > 0)
            {
                total_mass -= 1;
                objectTaken -= 1;
            }

            if (onecollision == false && (total_mass >= mass_accepted || collision.relativeVelocity.magnitude > magnitude_accepted))
            {

                    onecollision = true;
                    Destroy(gameObject);
                    Instantiate(brokenObject, transform.position, transform.rotation);
                    brokenObject.localScale = transform.localScale;
                    Vector3 explosionPos = transform.position;
                    Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);


                    foreach (Collider hit in colliders)
                    {
                        if (hit.GetComponent<Rigidbody>())
                        {
                            hit.GetComponent<Rigidbody>().AddExplosionForce(power * collision.relativeVelocity.magnitude, explosionPos, radius, upwards);
                        }
                    }
                
            }
        }
	}


    void OnCollisionExit(Collision collision)
    {
        if (!collision.collider.isTrigger && !collision.collider.CompareTag("Untagged"))
        {
            total_mass -= 1;

            if (collision.collider.CompareTag("Player"))
            {
                if (!collision.collider.GetComponent<PlayerState>().GetIsHandFree())
                {
                    objectTaken -= 1;
                    total_mass -= collision.rigidbody.mass;
                }
            }
        }
    }
}
