﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvertMagnetPolarity : MonoBehaviour {
    public void InvertPolarity () {
        GetComponent<Magnet_Script>().magnetDirection *= -1;
	}
}
