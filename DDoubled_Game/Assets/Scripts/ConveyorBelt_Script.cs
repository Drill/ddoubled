﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorBelt_Script : MonoBehaviour
{
    public Transform endpoint;
    public Transform endpoint2;
    public float speed; // 8 is a good value for a convejorbelt
    private bool direction_forward;

    ConveyorBelt_animation conv_animation;
    
    Material material_0;
    Material material_1;
    Material tempMaterial;




    // Use this for initialization
    void Awake()

    {
        
        conv_animation = GetComponent<ConveyorBelt_animation>();
        material_0 = GetComponent<Renderer>().materials[0];
        material_1 = GetComponent<Renderer>().materials[1];


        direction_forward = true;
     

    }

    // Update is called once per frame
    void Update()
    {

    }
    

    //public void EnableElement()
    //{
        
    //    direction_forward = false;
    //}

    //public void DisableElement()
    //{
        
    //    direction_forward = true;
    //}


    // the object that is on this object is transported forward
    void OnTriggerStay(Collider other)
    {
        if (!other.isTrigger && other.gameObject.transform.parent == null) {
            if (direction_forward)
                other.transform.position = Vector3.MoveTowards(other.transform.position, endpoint.position, speed * Time.deltaTime);
            else
                other.transform.position = Vector3.MoveTowards(other.transform.position, endpoint2.position, speed * Time.deltaTime);
        }
    }

   /* void OnTriggerExit(Collider other) {
        if (!other.isTrigger) {
            // add a little force to the object transported in order to make it leave the conveyor belt
            try {
                if (direction_forward)
                    other.GetComponent<Rigidbody>().AddForce(3 * (endpoint.transform.position - other.transform.position), ForceMode.Impulse);
                else
                    other.GetComponent<Rigidbody>().AddForce(3 * (endpoint2.transform.position - other.transform.position), ForceMode.Impulse);
            } catch (MissingComponentException e) {
                Debug.Log("The object that has left the conveyor belt doesn't have a rigidbody");
            }
        }
    }*/

    public void ChangeDirection()
    {
        tempMaterial = new Material(material_1);
        //tempMaterial.mainTexture = material_1.mainTexture;
        material_1.mainTexture = material_0.mainTexture;
        material_0.mainTexture = tempMaterial.mainTexture;

        direction_forward =! direction_forward;
        conv_animation.scrolly *=-1;
    }


}
