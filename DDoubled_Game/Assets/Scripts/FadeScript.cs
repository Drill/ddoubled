﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeScript : MonoBehaviour {

    private Animator anim;

	// Use this for initialization
	private void Awake () {
        anim = GetComponent<Animator>();
	}

    private void OnEnable()
    {
        EventManager.StartListening("NextLevel", FadeStart);
    }

    private void FadeStart()
    {
        EventManager.StopListening("NextLevel", FadeStart);
        Invoke("FadeOut", 2f);
        Debug.Log("Fade Recive");
    }

    private void FadeOut()
    {
        Debug.Log("Fade Start");
        anim.SetTrigger("FadeOut");
    }
}
