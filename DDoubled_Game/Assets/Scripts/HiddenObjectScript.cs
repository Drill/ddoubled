﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiddenObjectScript : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            EventManager.TriggerEvent("HiddenObjectTaken");
            Debug.Log("Taken Hidden Object");
            gameObject.SetActive(false);
        }
    }
}
