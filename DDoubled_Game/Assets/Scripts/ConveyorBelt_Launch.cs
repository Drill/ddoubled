﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorBelt_Launch : MonoBehaviour {
    
    public Transform endpoint;
    public float resistance;   // low value = more speed

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    // apply a force to the object that is on this object in order to launch it forward
    void OnTriggerEnter(Collider other) {
        if(other.gameObject.transform.parent == null)
            try {
                other.GetComponent<Rigidbody>().AddForce(3 * (endpoint.position - other.transform.position) / resistance, ForceMode.Impulse);
            } catch (MissingComponentException e) {
                Debug.Log("The object on the conveyor belt doesn't have a rigidbody. Force is not applicated." + this.gameObject.name);
            }
    }

    void OnTriggerExit(Collider other) {
        if (other.gameObject.transform.parent == null)
            try {
                other.GetComponent<Rigidbody>().AddForce(-3 * (endpoint.position - other.transform.position) / (resistance), ForceMode.Impulse);
            } catch (MissingComponentException e) {
                Debug.Log("The object leaving the conveyor belt doesn't have a rigidbody. Force hasn't been applicated.");
            }
    }
}
