﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMovements : MonoBehaviour {
    public enum MovementAxes { Horizontal, Vertical };
    public enum MovementDirection { Up, Right, Down, Left };

    [System.Serializable]
    public struct LevelToReach {
        public Transform level;
        //public MovementAxes axis;
        public MovementDirection direction;
    }

    //[Header("Set which levels are reachable, along which axis and following which direction")]
    [Header("Set which levels are reachable and which direction is to follow")]
    public LevelToReach[] destinationLevels;

    [Header("Movement speed")]
    public float speed = 5;

    GameObject player;
    int nextLevelIndex;
    bool playerOnThisLevel;
    bool isMoving;

    void Awake() {
        player = GameObject.FindGameObjectWithTag("Player");
        playerOnThisLevel = false;
        isMoving = false;
    }

    // check if the input received allows a movement on the map
    void CheckInput() {
        if (Input.GetAxisRaw("Horizontal") > 0) {
            for (int i = 0; i < destinationLevels.Length; i++) {
                if (/*destinationLevels[i].axis.ToString() == "Horizontal" &&*/ destinationLevels[i].direction.ToString() == "Right") {
                    nextLevelIndex = i;
                    isMoving = true;
                    return;
                }
            }
        }

        if (Input.GetAxisRaw("Horizontal") < 0) {
            for (int i = 0; i < destinationLevels.Length; i++) {
                if (/*destinationLevels[i].axis.ToString() == "Horizontal" &&*/ destinationLevels[i].direction.ToString() == "Left") {
                    nextLevelIndex = i;
                    isMoving = true;
                    return;
                }
            }
        }


        if (Input.GetAxisRaw("Vertical") > 0) {
            for (int i = 0; i < destinationLevels.Length; i++) {
                if (/*destinationLevels[i].axis.ToString() == "Vertical" &&*/ destinationLevels[i].direction.ToString() == "Up") {
                    nextLevelIndex = i;
                    isMoving = true;
                    return;
                }
            }
        }

        if (Input.GetAxisRaw("Vertical") < 0) {
            for (int i = 0; i < destinationLevels.Length; i++) {
                if (/*destinationLevels[i].axis.ToString() == "Vertical" &&*/ destinationLevels[i].direction.ToString() == "Down") {
                    nextLevelIndex = i;
                    isMoving = true;
                    return;
                }
            }
        }
    }

    void OnTriggerEnter(Collider other) {
        playerOnThisLevel = true;
        player.GetComponent<Map_PlayerMovements>().SetIsMoving(false);
    }

    void OnTriggerExit(Collider other) {
        playerOnThisLevel = false;
        player.GetComponent<Map_PlayerMovements>().SetIsMoving(true);
    }

    public bool GetPlayerOnThisLevel() {
        return playerOnThisLevel;
    }

    void FixedUpdate()
    {
        if (playerOnThisLevel)
            CheckInput();

        // move towards the destination level
        if (isMoving && !destinationLevels[nextLevelIndex].level.gameObject.GetComponent<MapMovements>().GetPlayerOnThisLevel()) {
            Vector3 pos = Vector3.MoveTowards(player.transform.position, destinationLevels[nextLevelIndex].level.position, speed * Time.deltaTime);
            player.GetComponent<Rigidbody>().MovePosition(pos);
        // if the destination level is reached
        } else {
            isMoving = false;
        }

        //// move towards the destination level
        //if (isMoving && player.transform.position != destinationLevels[nextLevelIndex].level.position) {
        //    Vector3 pos = Vector3.MoveTowards(player.transform.position, destinationLevels[nextLevelIndex].level.position, speed * Time.deltaTime);
        //    player.GetComponent<Rigidbody>().MovePosition(pos);
        //    // if the destination level is reached
        //} else if (player.transform.position == destinationLevels[nextLevelIndex].level.position)
        //    isMoving = false;
    }
}
