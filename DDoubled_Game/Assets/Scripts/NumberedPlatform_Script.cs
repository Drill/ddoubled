﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberedPlatform_Script : MonoBehaviour {

    private List<GameObject> objects=new List<GameObject> ();   // objects on the platform
    List<string> accepted_tags = new List<string>();            // objects that can be carried
    private int counter=0;
    public int pg_number;   // The number of characters required to activate the movement
    public Transform start_pos;
    private Vector3 start_pos_original;
    public Transform final_pos;
    private Vector3 final_pos_original;
    public float speed;
    [Header("Check this if the platform starts attached to the floor or a wall")]
    public bool startsAttachedToSomething;
    private bool isMoving;

    //private Animator animator;


    void Awake ()
    {
        // Store the original values of start_pos and final_pos
        start_pos_original = start_pos.position;
        final_pos_original = final_pos.position;
        isMoving = false;

        // objects in the game that can be carried by the numbered platform
        accepted_tags.Add("Player");
        accepted_tags.Add("MetalBox");
        accepted_tags.Add("WoodBox");
        accepted_tags.Add("Sphere");

        //player = GameObject.FindGameObjectWithTag("Player");
        //animator = this.GetComponent<Animator>();
    }



    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && other.gameObject.transform.parent == null)
            counter += 1;

        if (accepted_tags.Contains(other.gameObject.tag) && other.gameObject.transform.parent == null) {
            objects.Add(other.gameObject);
            Debug.Log("Character added. Counter is: " + counter);
        }

        if (counter >= pg_number) {
            foreach(GameObject obj in objects)
                if (other.gameObject.transform.parent == null)
                    obj.transform.parent = transform;   
        }
     }




    private void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "Player")
            counter -= 1;

        if (accepted_tags.Contains(other.gameObject.tag)) {
            objects.Remove(other.gameObject);
            Debug.Log("Character subtracted. Counter is: " + counter);
        }

        if (other.gameObject.transform.parent == transform) {
            other.transform.parent = null;
        }
    }


    
    void FixedUpdate () {
        //  if the platform doesn't start attached to the floor or a wall
        if (!startsAttachedToSomething)
        {
            // if the platform has to move towards the endPoint, but it isn't still there and if there aren't obstacles on the path
            if ((counter >= pg_number && transform.position != final_pos_original) && this.GetComponentInChildren<ObstacleDetector_Script>().NumberOfObjectsInRange() < 1
                ||
                // if the platform has to move towards the startPoint, but it isn't still there
                (counter < pg_number && transform.position != start_pos_original))
            {

                isMoving = true;

            }
            else
                isMoving = false;

            //  if the platform starts attached to the floor or a wall
        }
        else
        {
            // if the platform has to move towards the endPoint, but it isn't still there
            if ((counter >= pg_number && transform.position != final_pos_original)
            ||
            // if the platform has to move towards the startPoint, but it isn't still there and if there aren't obstacles on the path
            (counter < pg_number && transform.position != start_pos_original) && this.GetComponentInChildren<ObstacleDetector_Script>().NumberOfObjectsInRange() < 1)
            {

                isMoving = true;

            }
            else
                isMoving = false;
        }


        if (counter >= pg_number && isMoving)
            this.gameObject.transform.position = Vector3.MoveTowards(this.gameObject.transform.position, final_pos_original, speed * Time.deltaTime);

        if (counter < pg_number && isMoving)
            this.gameObject.transform.position = Vector3.MoveTowards(this.gameObject.transform.position,start_pos_original, speed * Time.deltaTime);
    }
}
