﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorElevatorListener : MonoBehaviour {
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("CollisionFloorDetect");
            EventManager.TriggerEvent("StartMovementElevator");
        }
    }
}
