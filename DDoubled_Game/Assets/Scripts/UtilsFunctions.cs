﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UtilsFunctions : MonoBehaviour
{
    // find a child of a game object by tag (just the first one)
    public static GameObject FindGameObjectInChildWithTag(GameObject parent, string tag) {
        Transform t = parent.transform;

        for (int i = 0; i < t.childCount; i++) {
            if (t.GetChild(i).gameObject.tag == tag)
                return t.GetChild(i).gameObject;
        }

        return null;
    }

    // find a child of a game object by layer (just the first one)
    public static GameObject FindGameObjectInChildWithLayer(GameObject parent, string layer) {
        Transform t = parent.transform;

        for (int i = 0; i < t.childCount; i++) {
            if (t.GetChild(i).gameObject.layer == LayerMask.NameToLayer(layer))
                return t.GetChild(i).gameObject;
        }

        return null;
    }

    // find a game object by tag (just the first one) in the gameObject passed or in its children
    public static GameObject FindGameObjectInThisObjOrChildWithTag(GameObject parent, string tag) {
        if (parent.tag == tag)
            return parent;

        Transform t = parent.transform;

        for (int i = 0; i < t.childCount; i++) {
            if (t.GetChild(i).gameObject.tag == tag)
                return t.GetChild(i).gameObject;
        }

        return null;
    }

    // find a game object by layer (just the first one) in the gameObject passed or in its children
    public static GameObject FindGameObjectInThisObjOrChildWithLayer(GameObject parent, string layer) {
        if (parent.layer == LayerMask.NameToLayer(layer))
            return parent;

        Transform t = parent.transform;

        for (int i = 0; i < t.childCount; i++) {
            if (t.GetChild(i).gameObject.layer == LayerMask.NameToLayer(layer))
                return t.GetChild(i).gameObject;
        }

        return null;
    }
}
