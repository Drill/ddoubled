﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DDouble_Script : MonoBehaviour {
    bool used;

	void Awake () {
        used = false;
	}

    public void UseDDouble() {
        used = true;
    }

    public bool IsUsed() {
        return used;
    }
}
