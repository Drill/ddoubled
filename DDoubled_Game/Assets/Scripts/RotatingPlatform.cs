﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RotatingPlatform : MonoBehaviour
{
    
    private int up = 0;
    public float initial_degrees;
    public float rotation_degree;
    float y_degree;
    float z_degree;
   
   
    void Awake()
     {
        y_degree = transform.rotation.eulerAngles.y;
        z_degree = transform.rotation.eulerAngles.z; 
    }


    public void EnableElement()
    {
        up = 1;
        Debug.Log(up);
    }

    public void DisableElement()
    {
        up = 2;
        Debug.Log(up);
    }

    void FixedUpdate()
    {

        if (up.Equals(1))
        {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(initial_degrees+rotation_degree, y_degree, z_degree), 100f * Time.deltaTime);
               
        }

        else if (up.Equals(2))
        {
            if(initial_degrees==180)
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, y_degree, z_degree), 100f * Time.deltaTime);
            else transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(initial_degrees, y_degree, z_degree), 100f * Time.deltaTime);
        }
    }
}



