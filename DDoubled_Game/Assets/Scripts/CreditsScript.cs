﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditsScript : MonoBehaviour
{
    public float setTime;

    private float totalTime;

    // Start is called before the first frame update
    private void Awake()
    {
        GameManager.instance.MusicControl(45);
        totalTime = setTime;
    }

    // Update is called once per frame
    private void Update()
    {
        totalTime -= Time.deltaTime;
        if (totalTime <= 0)
            SceneManager.LoadScene(2);
    }
}
