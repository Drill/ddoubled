﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BeforeMenu : MonoBehaviour
{
    public float setTime;

    private float totalTime;
    private Animator anim;


    // Start is called before the first frame update
    private void Awake()
    {
        anim = GetComponent<Animator>();
        totalTime = setTime;
    }

    // Update is called once per frame
    private void Update()
    {
        totalTime -= Time.deltaTime;
        if (totalTime <= 0)
        {
            anim.SetTrigger("FadeOut");
            Invoke("ChangeScene", 1.5f);
        }
           
    }

    private void ChangeScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
