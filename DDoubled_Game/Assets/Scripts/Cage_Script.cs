﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cage_Script : MonoBehaviour {
  
    [Header("Fall Sound")] public AudioClip FallSound;
    [Header("Movement Sound")] public AudioClip MovementSound;

    private AudioSource CageSoundEffect;
    private Rigidbody rigidbody_cage;
    private BoxCollider boxCollider;
    private Vector3 lastPosition;
    private bool isGrounded;
    public LayerMask groundLayer;   // the layer on which the cage produces the bumping sound

    private void Awake()
    {
        rigidbody_cage = GetComponent<Rigidbody>();
        boxCollider = GetComponent<BoxCollider>();
        groundLayer = LayerMask.GetMask("Default");
    }

    private void OnEnable()
    {
        isGrounded = false;
        CageSoundEffect = gameObject.GetComponent<AudioSource>();

    }

    private void OnCollisionEnter(Collision collision) {
        // if the cage was in air and now it is on the floor
        if (!isGrounded && Bumped())
        {
            CageSoundEffect.Play();
            isGrounded = true;
        }
    }

    private void OnCollisionExit(Collision collision) {
        // if the cage is no more on the floor
        if (collision.gameObject.layer == LayerMask.NameToLayer("Default") && !Bumped()) {
            isGrounded = false;
        }
    }

    // check if the cage has fallen and has bumped the floor
    bool Bumped() {
        // return true if the collision with the specified layer happens vertically
        return Physics.CheckCapsule(boxCollider.bounds.center, new Vector3(boxCollider.bounds.center.x, boxCollider.bounds.min.y, boxCollider.bounds.center.z),
                                    boxCollider.contactOffset * 0.9f, groundLayer);
    }

    private void FixedUpdate()
    {
        Vector3 curPos = transform.position;
        if(isGrounded)
        if (curPos != lastPosition && !CageSoundEffect.isPlaying)
        {
            //CageSoundEffect.PlayOneShot(MovementSound);
        }
        lastPosition = curPos;
    }


    // activate the gravity on the cage at midair
    public void CageFall()
    {
        rigidbody_cage.useGravity = true;
        rigidbody_cage.isKinematic = false;
        //CageSoundEffect.PlayOneShot(FallSound);
        lastPosition = transform.position;
    }
}

