﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryExplosionScript : MonoBehaviour
{

    private float totalTime;
    private AudioSource audio;
    private bool go;

    private void Awake()
    {
        totalTime = 15;
        go = true;
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (go)
        {
            totalTime -= Time.deltaTime;
            if(totalTime <= 0)
            {
                audio.Play();
                go = false;
            }
        }
    }
}
