﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChangeDimension : MonoBehaviour {

    public void LoadNextMap(int indexOfDimensionToLoad)
    {
        if (GameManager.instance.CheckUnlockedDimension(indexOfDimensionToLoad)) {
            Cursor.visible = false;
            SceneManager.LoadScene(indexOfDimensionToLoad);
            GameManager.instance.MusicControl(indexOfDimensionToLoad);
        } else {
            // suono errore e apparizione scritta "locked"
        }
    }
}
