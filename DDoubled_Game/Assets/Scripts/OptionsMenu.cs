﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour {

    public AudioMixer audioMixer;
    public Resolution[] resolutions;
    public Dropdown resolutionUI;
    public Dropdown qualityUI;
    public Slider mainSoundUI;
    public Slider effectSoundUI;
    public Slider musicSoundUI;
    public bool fullScreen;



    private List<string> resolutionList = new List<string>();

	// Use this for initialization
	void Start () {
        int currentResolution = 0;
        resolutions = Screen.resolutions;
        resolutionUI.ClearOptions();
        for (int i = 0; i < resolutions.Length; i++)
        {
            if (resolutions[i].height.Equals(Screen.currentResolution.height) && 
                resolutions[i].width.Equals(Screen.currentResolution.width) && resolutions[i].ToString().Equals(GameManager.instance.resolution)) { 
                currentResolution = i;
                Screen.SetResolution(resolutions[i].width, resolutions[i].height, Screen.fullScreen);
            }
            else if(resolutions[i].height.Equals(Screen.currentResolution.height) &&
                resolutions[i].width.Equals(Screen.currentResolution.width) && (GameManager.instance.resolution == null))
                currentResolution = i;
            resolutionList.Add(resolutions[i].width + "x" + resolutions[i].height);
        }

        resolutionUI.AddOptions(resolutionList);
        resolutionUI.value = currentResolution;
        resolutionUI.RefreshShownValue();

        audioMixer.SetFloat("MainVolume", GameManager.instance.mainSoundLevel);
        audioMixer.SetFloat("EffectsVolume", GameManager.instance.effectSoundLevel);
        audioMixer.SetFloat("MusicVolume", GameManager.instance.musicSoundLevel);
        mainSoundUI.value = GameManager.instance.mainSoundLevel;
        musicSoundUI.value = GameManager.instance.musicSoundLevel;
        effectSoundUI.value = GameManager.instance.effectSoundLevel;

        QualitySettings.SetQualityLevel(GameManager.instance.quality);
        qualityUI.value = GameManager.instance.quality;
        qualityUI.RefreshShownValue();
        
        this.fullScreen = GameManager.instance.fullScreen;
        SetFullScreen();
    }
	
    public void SetMainVolume(float volume)
    {
        audioMixer.SetFloat("MainVolume",volume);
        GameManager.instance.mainSoundLevel = volume;
    }

    public void SetEffectsVolume(float volume)
    {
        audioMixer.SetFloat("EffectsVolume", volume);
        GameManager.instance.effectSoundLevel = volume;
    }

    public void SetMusicVolume(float volume)
    {
        audioMixer.SetFloat("MusicVolume", volume);
        GameManager.instance.musicSoundLevel = volume;
    }

    public void SetQuality(int quality)
    {
        QualitySettings.SetQualityLevel(quality);
        GameManager.instance.quality = quality;
    }

    public void SetFullScreen()
    {
        Screen.fullScreen = fullScreen;
        GameManager.instance.fullScreen = fullScreen;
    }

    public void SetResolution(int index)
    {
        Screen.SetResolution(resolutions[index].width, resolutions[index].height, Screen.fullScreen);
        GameManager.instance.resolution = resolutions[index].ToString();
        Debug.Log(GameManager.instance.resolution);
    }

    public void OnFullScreen()
    {
        fullScreen = true;
        SetFullScreen();
    }

    public void OffFullScreen()
    {
        fullScreen = false;
        SetFullScreen();
    }

    public void OnIsometricCorrection()
    {
        GameManager.instance.isometricMotionCorrection = true;
    }

    public void OffIsometricCorrection()
    {
        GameManager.instance.isometricMotionCorrection = false;
    }

    public void SaveBeforeBack()
    {
        SaveSystem.SaveData(GameManager.instance);
    }

}
