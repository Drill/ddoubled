﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Map_Circle_Level : MonoBehaviour {
    [Header("Number/name of the level")]
    public string stringToShowOnHUD;
    [Header("Offset of this scene relative to the index of the map scene inside the SceneBuilder")]
    public int sceneIndexOffset;
    public int index;
    public List<Sprite> usedSprite = new List<Sprite>();

    public bool isUnlocked;
    
    private TextMeshProUGUI dimensionName;
    private string originalHUDLabel;

    void Awake () {
        
        StoredDataScript data = SaveSystem.LoadData();
        if (data != null)
        {
            
            isUnlocked = data.level[index];
            
            if (isUnlocked)
            {
                this.gameObject.GetComponent<SpriteRenderer>().sprite = usedSprite[0];
            }
            else
            {
                this.gameObject.GetComponent<SpriteRenderer>().sprite = usedSprite[1];
            }
        }

        dimensionName = GameObject.FindGameObjectsWithTag("HUD")[0].GetComponentInChildren<TextMeshProUGUI>();
        originalHUDLabel = dimensionName.text;
    }

    

    public void StartLevel(int indexMapScene)
    {
        SceneManager.LoadScene(indexMapScene + sceneIndexOffset);
        GameManager.instance.MusicControl(indexMapScene + sceneIndexOffset);

    }

    private void OnTriggerEnter(Collider other)
    {
        dimensionName.text += " - " + stringToShowOnHUD;
    }

    private void OnTriggerExit(Collider other)
    {
        dimensionName.text = originalHUDLabel;
    }

    public bool GetIsUnlocked() {
        return isUnlocked;
    }
}
