﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map_PlayerMovements_Old : MonoBehaviour {

    private Vector3 movement;
    //private GameObject level = null;
    private Animator anim;
    private Rigidbody playerRigidBody;

    [Header("Character Speed")]
    public float speed = 6f;

    [Header("Character AudioSource")]
    public AudioSource audioSource;

    // Use this for initialization
    void Awake () {
        anim = GetComponent<Animator>();
        playerRigidBody = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	private void FixedUpdate () {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");
        //if (level != null && Input.GetButtonDown("Jump"))
        //{
        //    level.GetComponent<Map_Circle_Level>().StartLevel();
        //}
        //else
        //{
            if(h == 0 && v == 0)
            {
                anim.SetBool("IsWalking", false);
            }
            else
            {
                Moving(h, v);
                Turning(h , v);
            }
        //}
	}

    private void Turning(float h, float v)
    {
        transform.rotation = Quaternion.LookRotation(movement);
    }

    private void Moving(float h, float v)
    {
        movement.Set(h, 0f, v);
        movement = movement.normalized * speed * Time.deltaTime;
        playerRigidBody.MovePosition(transform.position + movement);
        anim.SetBool("IsWalking", true);
        if(!audioSource.isPlaying)
            audioSource.Play();
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if(other.gameObject.tag == "Level")
    //        level = other.gameObject;
    //}

    //private void OnTriggerExit(Collider other)
    //{
    //    if (other.gameObject.tag == "Level")
    //        level = null;
    //}
}
