﻿using UnityEngine;
using System.Collections;

public class PickAndDropObject : MonoBehaviour
{
    private GameObject objectToPick_Drop;
    private Key_Script keyScript = null;
    private Sphere_Script sphereScript = null;

    //private readonly object _syncRoot = new object();   // object used in the synchronized method for the interaction of the character with the objects

    public void OnEnable()
    {
        EventManager.StartListening("PickUp", PickUp);
        EventManager.StartListening("DropDown", DropDown);
        EventManager.StartListening("RemoveObject", RemoveObject);
    }


    public void Awake()
    {
        if (gameObject.CompareTag("Key"))
        {
            keyScript = GetComponent<Key_Script>();

        }else if (gameObject.CompareTag("Sphere"))
        {
            sphereScript = GetComponent<Sphere_Script>();
        }
    }
   
    public void PickUp()
    {
        // lock this method to avoid weird behaviours
        //lock (_syncRoot) {
            // Pick Up of the Key near The Player
            if (gameObject.CompareTag("Key")) {
                // check if the key is near the player
                if (keyScript.playerDetected) {
                    // set the key as taken and the key becomes a child of the player
                    keyScript.setHandTransform(keyScript.player.transform.Find("Hand").GetComponent<Transform>());
                    keyScript.keyTaken = true;
                    gameObject.transform.parent = keyScript.getHandTransform();
                    keyScript.key_Rigidbody.useGravity = false;

                }

            } else if (gameObject.CompareTag("Sphere")) {
                                                    // you can't pick up a shpere if it is attracted by a manget
                if (sphereScript.playerDetected && !gameObject.GetComponent<AttractionRepellion>().isUnderMagnetInfluence()) {
                    // set the key as taken and the key becomes a child of the player
                    sphereScript.setHandTransform(sphereScript.player.transform.Find("Hand").GetComponent<Transform>());
                    sphereScript.sphereTaken = true;
                    gameObject.transform.parent = sphereScript.getHandTransform();
                    sphereScript.sphere_Rigidbody.useGravity = false;

                }
            }
        //}
    }

    public void DropDown()
    {
        // put down the Key near The Player
        if (gameObject.CompareTag("Key"))
        {
            // set the key as not taken and the key isn't a child of the player
            keyScript.keyTaken = false;
            gameObject.transform.parent = null;
            keyScript.setHandTransform(null);
            keyScript.player = null;
            keyScript.key_Rigidbody.useGravity = true;
        }
               
        if (gameObject.CompareTag("Sphere"))
        {
            
            sphereScript.sphereTaken = false;
            gameObject.transform.parent = null;
            sphereScript.setHandTransform(null);
            sphereScript.player = null;
            sphereScript.sphere_Rigidbody.useGravity = true;
        }
    }
    public void RemoveObject()
    {
        if (gameObject.CompareTag("Key"))
        {
            // set the key as not taken and the key isn't a child of the player
            keyScript.keyTaken = false;
            gameObject.transform.parent = null;
            keyScript.setHandTransform(null);
            keyScript.player = null;
            keyScript.key_Rigidbody.useGravity = true;
            transform.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward);
        }

        if (gameObject.CompareTag("Sphere"))
        {

            sphereScript.sphereTaken = false;
            gameObject.transform.parent = null;
            sphereScript.setHandTransform(null);
            sphereScript.player = null;
            sphereScript.sphere_Rigidbody.useGravity = true;
        }
    }
}
