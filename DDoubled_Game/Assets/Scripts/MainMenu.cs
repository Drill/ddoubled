﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public Button continueGame;
    public Button options;
    public Button credits;
    public Button exitGame;

    public void Start()
    {
        Cursor.visible = true;
        if (GameManager.instance.alreadyStartAgame)
        {
            continueGame.gameObject.SetActive(true);
            options.transform.position = new Vector3(options.transform.position.x, options.transform.position.y - ((61f/600) * Screen.height), options.transform.position.z);
            credits.transform.position = new Vector3(credits.transform.position.x, credits.transform.position.y - ((61f/600) * Screen.height), credits.transform.position.z);
            exitGame.transform.position = new Vector3(exitGame.transform.position.x, exitGame.transform.position.y - ((61f/600) * Screen.height), exitGame.transform.position.z);
        }
    }

    public void PlayGame()
    {
        Cursor.visible = false;
        string path = Application.persistentDataPath + "/ddoubleData.data";
        File.Delete(path);
        GameManager.instance.LoadGame();
        GameManager.instance.alreadyStartAgame = true;
        SaveSystem.SaveData(GameManager.instance);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        GameManager.instance.StopMusic();
        NewGameSet(); 
    }

    public void ContinueGame()
    {
        SceneManager.LoadScene(4);
    }

    public void ExitGame()
    {
        SaveSystem.SaveData(GameManager.instance);
        Application.Quit();
    }

    public void NewGameSet()
    {
        for (int i = 0; i < GameManager.instance.savedLevel.Length; i++)
        {
            switch (i)
            {
                /*case 0:
                    GameManager.instance.savedLevel[i] = true;
                    break;
                case 1:
                    GameManager.instance.savedLevel[i] = true;
                    break;
                case 2:
                    GameManager.instance.savedLevel[i] = true;
                    break;
                case 3:
                    GameManager.instance.savedLevel[i] = true;
                    break;
                case 4:
                    GameManager.instance.savedLevel[i] = true;
                    break;
                case 17:
                    GameManager.instance.savedLevel[i] = true;
                    break;
                case 24:
                    GameManager.instance.savedLevel[i] = true;
                    break;
                default:
                    GameManager.instance.savedLevel[i] = false;
                    break;*/
                case 0:
                    GameManager.instance.savedLevel[i] = true;
                    break;
                case 1:
                    GameManager.instance.savedLevel[i] = true;
                    break;
                case 2:
                    GameManager.instance.savedLevel[i] = true;
                    break;
                case 3:
                    GameManager.instance.savedLevel[i] = true;
                    break;
                case 4:
                    GameManager.instance.savedLevel[i] = true;
                    break;
                case 5:
                    GameManager.instance.savedLevel[i] = true;
                    break;
                case 6:
                    GameManager.instance.savedLevel[i] = true;
                    break;
                case 12:
                    GameManager.instance.savedLevel[i] = true;
                    break;
                case 22:
                    GameManager.instance.savedLevel[i] = true;
                    break;
                
                case 33:
                    GameManager.instance.savedLevel[i] = true;
                    break;
                
                default:
                    GameManager.instance.savedLevel[i] = false;
                    break;
            }
        }
    }

}
