﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key_Script : MonoBehaviour {


    [Header("Use Key Sound")] public AudioClip UseKeySound;
    
    private Transform hand;
    private AudioSource keySoundEffect;
    public Rigidbody key_Rigidbody;
    public bool playerDetected;
    public bool keyTaken;
    private bool keyUsed;
    public GameObject player;
    

	// Use this for initialization
	private void Awake() {

        key_Rigidbody = GetComponent<Rigidbody>();
        keySoundEffect = GetComponent<AudioSource>();
        key_Rigidbody.useGravity = true;
        keyTaken = false;
        keyUsed = false;
        playerDetected = false;
        hand = null;
        player = null;
        

    }
	
	// Update is called once per frame
	private void Update () {
        if (keyTaken)
        {
            transform.position = hand.position;
            transform.rotation = hand.rotation* Quaternion.AngleAxis(90, new Vector3(-1, 0, 0)) * Quaternion.AngleAxis(90, new Vector3(0,0,1));

        } else if (!keyTaken)
        {
            if (!key_Rigidbody.useGravity)
            {
                hand = null;
            }
        }
	}

    public void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Player"))
        {
                player = other.gameObject;
                playerDetected = true;
        }

    }

    private void OnCollisionExit(Collision other)
    {
        if (other.collider.CompareTag("Player"))
        {
            playerDetected = false;

        }else if (other.collider.CompareTag("Floor"))
        {
            keySoundEffect.Play();
        }
    }

    public Transform getHandTransform()
    {
        return hand;
    }

    internal void setHandTransform(Transform newOne)
    {
       hand=newOne;
    }

    public void KeyUsed()
    {
        if(keyUsed)
            Destroy(gameObject);
    } 
}
