﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StoredDataScript
{
    public bool[] level;
    public string resolution;
    public int screenQuality;
    public bool fullScreen;
    public float masterSoundLevel;
    public float effectSoundLevel;
    public float musicSoundLevel;
    public bool alreadyStartAgame;
    public bool isometricMotionCorrection;
    public bool[] Dimension;



    public StoredDataScript(GameManager gameManager)
    {
        this.level = gameManager.savedLevel;
        this.fullScreen = gameManager.fullScreen;
        this.masterSoundLevel = gameManager.mainSoundLevel;
        this.musicSoundLevel = gameManager.musicSoundLevel;
        this.effectSoundLevel = gameManager.effectSoundLevel;
        this.screenQuality = gameManager.quality;
        this.resolution = gameManager.resolution;
        this.Dimension = gameManager.Dimension;
        this.isometricMotionCorrection = gameManager.isometricMotionCorrection;
        this.alreadyStartAgame = gameManager.alreadyStartAgame;
    }

}
