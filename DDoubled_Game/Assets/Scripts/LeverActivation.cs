﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverActivation : MonoBehaviour
{

    public List<GameObject> objsToActivate;
    public float activeTime;

    private Animator anim;
    private bool collisionDetect = false;
    private bool isActivated = false;
    private float time;
    private AudioSource leverSound;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        leverSound = GetComponent<AudioSource>();
        time = activeTime;
    }

    private void Update()
    {
        if (isActivated)
        {
            time -= Time.deltaTime;
            if (time <= 0)
                DeactivatedLever();
        }
    }

    private void OnEnable()
    {
        EventManager.StartListening("MechanismActivation", ActivatedLever);
    }

    private void ActivatedLever()
    {
        if (collisionDetect && !isActivated)
        {
            isActivated = true;
            leverSound.Play();
            anim.SetBool("isActivated", true);
            foreach (GameObject gO in objsToActivate)
            {
                switch (gO.tag)
                {
                    case "Elevator":
                        gO.GetComponent<Elevator_Script>().EnableElement();
                        break;
                    case "Magnet":
                        gO.GetComponent<Magnet_Script>().InvertDirection();
                        break;
                    case "ConveyorBelt":
                        gO.GetComponent<ConveyorBelt_Script>().ChangeDirection();
                        break;
                    case "Door":
                        gO.GetComponent<Door_Script>().OpenDoor();
                        break;
                    case "RotatingWall":
                        gO.GetComponent<Door_Script>().OpenDoor();
                        break;
                    case "MovingPlatform":
                        gO.GetComponent<MovingPlatform_Script>().MovePlatform();
                        break;
                    case "RotatingPlatform":
                        gO.GetComponent<RotatingPlatform>().EnableElement();
                        break;
                    case "Cage":
                        gO.GetComponent<Cage_Script>().CageFall();
                        break;
                    case "Moving_wall":
                        gO.GetComponent<Moving_walls>().EnableElement();
                        break;
                    case "Portal":
                        gO.GetComponent<PortalTeleport>().ToggleIsEntry();
                        break;
                }
            }
        }
        else if (collisionDetect && isActivated)
        {
            DeactivatedLever();
        }
    }

    private void DeactivatedLever()
    {
        time = activeTime;
        isActivated = false;
        leverSound.Play();
        anim.SetBool("isActivated", false);
        foreach (GameObject gO in objsToActivate)
        {
            switch (gO.tag)
            {
                case "Elevator":
                    gO.GetComponent<Elevator_Script>().DisableElement();
                    break;
                case "Magnet":
                    gO.GetComponent<Magnet_Script>().InvertDirection();
                    break;
                case "ConveyorBelt":
                    gO.GetComponent<ConveyorBelt_Script>().ChangeDirection();
                    break;
                case "Door":
                    gO.GetComponent<Door_Script>().CloseDoor();
                    break;
                case "RotatingWall":
                    gO.GetComponent<Door_Script>().CloseDoor();
                    break;
                case "MovingPlatform":
                    gO.GetComponent<MovingPlatform_Script>().MovePlatform();
                    break;
                case "RotatingPlatform":
                    gO.GetComponent<RotatingPlatform>().DisableElement();
                    break;
                case "Cage":
                    gO.GetComponent<Cage_Script>().CageFall();
                    break;
                case "Moving_wall":
                    gO.GetComponent<Moving_walls>().DisableElement();
                    break;
                case "Portal":
                    gO.GetComponent<PortalTeleport>().ToggleIsEntry();
                    break;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            collisionDetect = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            collisionDetect = false;
        }
    }
}
                    