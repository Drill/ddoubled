﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator_Script : MonoBehaviour {

    [Header("Height of elevetor' movement")]
    public float stopYPosition;

    [Header("Time to wait before go down")]
    public float timeToWait;

    //Element that represents the gears
    //public GameObject cylinder;

    private bool move;
    private float startYPosition;
    private float time;


    // Use this for initialization
    private void Awake () {
        move = false;
        time = timeToWait;
        startYPosition = transform.position.y;
    }

    /* //Initialization of trigger listener
    private void OnEnable()
    {
        EventManager.StartListening("StartMovementElevator", StartMovementElevator);
    }*/

    //Call metod by an EventManager.TriggerEvent that start the movement execution
    public void EnableElement()
    {
        //EventManager.StopListening("StartMovementElevator", StartMovementElevator);
        move = true;
    }

    public void DisableElement()
    {
        //EventManager.StopListening("StartMovementElevator", StartMovementElevator);
        move = false;
    }

    // Update is called once per frame
    private void Update () { 
        if (move)
        {
            if (transform.transform.position.y < stopYPosition && time == timeToWait)
            {
                transform.position += transform.up * Time.deltaTime;
                Debug.Log("GoUp");
                //Part of code to add in case we want to implement also the gears
                //cylinder.transform.localPosition -= transform.up * Time.deltaTime;
                //cylinder.transform.Rotate(new Vector3(0, 15, 0) * Time.deltaTime);
            }
            else if(time <= 0 && transform.transform.position.y > startYPosition)
            {
                transform.position -= transform.up * Time.deltaTime;
                Debug.Log("GoDown");
            }
            else if(transform.transform.position.y <= startYPosition)
            {
                Debug.Log("Reset");
                move = false;
                time = timeToWait;
            }
            else
            {
                time -= Time.deltaTime;
                Debug.Log("Wainting");
            }
        }
    }
}
