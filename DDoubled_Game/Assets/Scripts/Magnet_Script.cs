﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet_Script : MonoBehaviour {
    List<GameObject> objsInRange;

    [Header("Streght of the Attraction and Repulsion")]
    public float magnetStrength;
    [Header("Attraction = 1; Repulsion = -1")]
    public int magnetDirection = 1;
    [Header("Check this if the magnet is strong enough to win against gravity")]
    public bool noGravity = false;      // true if the magnet is able to win against gravity; false by default
    
    //float distanceStretch = 10f;    // the force of the magnet depends on the distance between it and the object attracted

    void Awake () {
        objsInRange = new List<GameObject>();

        if (magnetDirection == 1) {
            this.transform.GetChild(0).gameObject.SetActive(false);
            this.transform.GetChild(1).gameObject.SetActive(true);
        } else {
            this.transform.GetChild(0).gameObject.SetActive(true);
            this.transform.GetChild(1).gameObject.SetActive(false);
        }
    }

    // when an object enters in the magnet's action range
    void OnTriggerEnter(Collider other) {
        // if the object is made of iron or if someone is carrying an iron object
        if (UtilsFunctions.FindGameObjectInThisObjOrChildWithLayer(other.gameObject, "Iron") != null && !other.isTrigger) {
            GameObject ironObj = UtilsFunctions.FindGameObjectInThisObjOrChildWithLayer(other.gameObject, "Iron");
            objsInRange.Add(ironObj);
            ironObj.GetComponent<AttractionRepellion>().MagnetInfluence(this.gameObject, this.magnetStrength, this.magnetDirection, this.noGravity /*, this.distanceStretch*/);
        }
    }

    // when an object exits from the magnet's action range
    void OnTriggerExit(Collider other) {
        // if the object is made of iron or if someone is carrying an iron object
        if (UtilsFunctions.FindGameObjectInThisObjOrChildWithLayer(other.gameObject, "Iron") != null && !other.isTrigger) {
            GameObject ironObj = UtilsFunctions.FindGameObjectInThisObjOrChildWithLayer(other.gameObject, "Iron");
            objsInRange.Remove(ironObj);
            ironObj.GetComponent<AttractionRepellion>().NoMoreMagnetInfluence(this.gameObject);
        }
    }

    // tell to all objects under this magnet's influence that the direction is changed
    public void InvertDirection() {
        magnetDirection *= -1;
        this.transform.GetChild(0).gameObject.SetActive(!this.transform.GetChild(0).gameObject.activeSelf);
        this.transform.GetChild(1).gameObject.SetActive(!this.transform.GetChild(1).gameObject.activeSelf);

        foreach (GameObject obj in objsInRange) {
            obj.GetComponent<AttractionRepellion>().PolarityInversion();
        }
    }
}
