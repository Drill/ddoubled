﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorBelt_animation : MonoBehaviour {
    public float scrollx;
    public float scrolly;
    Material correct_material;

    AudioSource conveyorBeltSound;

    void Awake() {
        conveyorBeltSound = GetComponent<AudioSource>();
        conveyorBeltSound.Play();
    }



    // Update is called once per frame
    void Update () {

        //TRASLATION
        float offsetx = Time.time * scrollx;
        float offsety = Time.time * scrolly;
        correct_material = GetComponent<Renderer>().materials[1];
        correct_material.mainTextureOffset= new Vector2(offsetx, offsety);


    }
}
