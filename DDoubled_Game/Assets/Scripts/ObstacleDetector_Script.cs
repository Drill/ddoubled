﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleDetector_Script : MonoBehaviour {
    int objectsInRange;
    List<string> accepted_tags = new List<string>();

    void Awake() {
        // objects in the game that can be in the path of a moving object
        accepted_tags.Add("Player");
        accepted_tags.Add("MetalBox");
        accepted_tags.Add("WoodBox");
        accepted_tags.Add("Sphere");

        objectsInRange = 0;
    }

    void OnTriggerEnter(Collider other) {
        if (accepted_tags.Contains(other.gameObject.tag)) {
            objectsInRange += 1;
            Debug.Log("Obstacle detected");
        }
    }

    void OnTriggerExit(Collider other) {
        if (accepted_tags.Contains(other.gameObject.tag)) {
            objectsInRange -= 1;
            Debug.Log("Obstacle no more detected");
        }
    }

    public int NumberOfObjectsInRange() {
        return objectsInRange;
    }
}
