﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

    public float dampTime = .2f;
    public float screenEdfeBuffer = 4f;
    public float minSize = 6.5f;
    public float minMotion = 0.1f;
    public float timeToReachTarget = 4f;
    

    [Header("Players")]
    public List<Transform> targets;

    private Camera camera;
    private float zoomSpeed;
    private Vector3 moveVelocity;
    private Vector3 desiredPosition;
    private Vector3 startPosition;
    private GameObject door;
    private bool startGame;
    private float countDown;
    private float startZoomMotion;
    private float startZoom = 3f;


    public void Awake()
    {
        camera = GetComponentInChildren<Camera>();
        try {
            gameObject.transform.position = GameObject.FindGameObjectWithTag("Door").transform.position;
            targets[0].gameObject.GetComponent<PlayerMovements>().enabled = false;
            startGame = false;
            countDown = timeToReachTarget;
            camera.orthographicSize = startZoom;
            startZoomMotion = startZoom;
        }
        catch (NullReferenceException)
        {
            Debug.Log("Doesn't exist a door");
            startGame = true;
            Debug.Log("Gooooo");
            EventManager.TriggerEvent("StartTheGame");
        }
    }

    private void FixedUpdate()
    {
        if (startGame) { 
            Move();
            Zooom();
        }else if (!startGame && camera.transform.position.x != targets[0].position.x && camera.transform.position.y != targets[0].position.y)
        {
            if(countDown >= timeToReachTarget-1f)
            {
                startZoomMotion += .01f;
                camera.orthographicSize = startZoomMotion;
            }
            else
            {
                float time = Time.deltaTime / timeToReachTarget;
                transform.position = Vector3.Lerp(gameObject.transform.position, targets[0].position, time);
            } 
        }
    }

    private void Update()
    {
        if(!startGame && countDown > 0)
            countDown -= Time.deltaTime;
        else if(!startGame && countDown <= 0)
        {
            startGame = true;
            Debug.Log("Gooooo");
            EventManager.TriggerEvent("StartTheGame");
        }
    }

    private void Move()
    {
        FindAvaragePosition();
        transform.position = Vector3.SmoothDamp(transform.position, desiredPosition, ref moveVelocity, dampTime);
    }

    private void Zooom()
    {
        float requireSize = FindRequireSize();
        camera.orthographicSize = Mathf.SmoothDamp(camera.orthographicSize, requireSize, ref zoomSpeed, dampTime);
    }

    private float FindRequireSize()
    {
        Vector3 desideredLocalPos = transform.InverseTransformPoint(desiredPosition);
        float size = 0f;

        for(int i = 0; i < targets.Count; i++)
        {
            Vector3 targetLocalPos = transform.InverseTransformPoint(targets[i].position);
            Vector3 desiredPosToTarget = targetLocalPos - desideredLocalPos;

            if (!targets[i].gameObject.activeSelf)
                continue;
            size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.y));
            size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.x) / camera.aspect);
        }
        size += screenEdfeBuffer;
        size = Mathf.Max(size, minSize);

        return size;
    }

    private void FindAvaragePosition()
    {
        Vector3 avaragePos = new Vector3();
        int numTargets = 0;

        for(int i = 0; i < targets.Count; i++)
        {
            if (!targets[i].gameObject.activeSelf)
                continue;
            avaragePos += targets[i].position;
            numTargets++;
        }
        if (numTargets > 0)
            avaragePos /= numTargets;
        avaragePos.y = transform.position.y;
        desiredPosition = avaragePos;
    }

    public void SetStarPositionAndSize()
    {
        // FindAvaragePosition();
        // transform.position = desiredPosition;
        // camera.orthographicSize = FindRequireSize();
    }

}
