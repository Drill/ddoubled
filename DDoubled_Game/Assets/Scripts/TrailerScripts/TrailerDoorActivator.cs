﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailerDoorActivator : MonoBehaviour
{
    [Header("Time Before Activate")]
    public float time;

    [Header("DoorToActivate")]
    public GameObject doorR;
    [Header("DoorToActivate")]
    public GameObject doorL;

    private float countDown;

    // Start is called before the first frame update
    void Awake()
    {
        countDown = time;   
    }

    // Update is called once per frame
    void Update()
    {
        countDown -= Time.deltaTime;
        if(countDown <= 0)
        {
            doorR.GetComponent<Door_Script>().OpenDoor();
            doorL.GetComponent<Door_Script>().OpenDoor();
        }
    }
}
