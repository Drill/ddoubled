﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Map_LevelSelectorScript : MonoBehaviour {
    GameObject level = null;
    GameObject player;
    int indexMapScene;  // the index of the current map inside the SceneBuilder

    void Awake() {
        player = GameObject.FindGameObjectWithTag("Player");
        indexMapScene = SceneManager.GetActiveScene().buildIndex;
    }
    void FixedUpdate () {
        transform.position = player.transform.position + new Vector3(0, -0.1f, 0); ;

        if (level != null &&
            (Input.GetButtonDown("Jump") || Input.GetKeyDown(KeyCode.Return))) {

            if (level.GetComponent<Map_Circle_Level>().GetIsUnlocked())
                level.GetComponent<Map_Circle_Level>().StartLevel(indexMapScene);
            else
                return; // suono errore e apparizione scritta "locked"
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Level")
            level = other.gameObject;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Level")
            level = null;
    }
}
