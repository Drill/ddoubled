﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState : MonoBehaviour {

    private bool handFree; 

    public void Awake()
    {
        handFree = true;
    }

    public void SetHandFree(bool flag)
    {
        handFree = flag;
        
    }

    public bool GetIsHandFree()
    {
        return handFree;
    }

    public void CloneState(PlayerState state)
    {
        SetHandFree(state.GetIsHandFree());
    }
}
