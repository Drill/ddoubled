﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Storyscene : MonoBehaviour
{
    public float setTime;
    public Text text;

    private float totalTime;
    private Animator anim;
    private int count;
    


    // Start is called before the first frame update
    private void Awake()
    {
        anim = GetComponent<Animator>();
        totalTime = setTime;
        count = 0;
    }

    // Update is called once per frame
    private void Update()
    {
        totalTime -= Time.deltaTime;
        if (totalTime <= 0)
        {
            anim.SetBool("FadeOut", true);
            if (count != 3)
            {
                Invoke("ChangeText", 1f);
                count += 1;
                totalTime = setTime;
            }
            else
            {
                Invoke("ChangeScene", 1.5f);
            }
        }

    }

    private void ChangeText()
    {
        switch (count)
        {
            case 1:
                text.text = "Especially if that thing is a machine to travel between dimensions.";
                break;
            case 2:
                text.text = "This is the reason why you will explore new dimensions...";
                break;
            case 3:
                text.text = "...and here your journey begins...";
                break;
        }
        anim.SetBool("FadeOut", false);
    }

    private void ChangeScene()
    {
        //EventManager.TriggerEvent("NextLevel");
        SceneManager.LoadScene(6);
        GameManager.instance.MusicControl(6);
    }
}


