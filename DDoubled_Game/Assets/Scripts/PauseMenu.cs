﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour {

    public static bool pauseFlag = false;
    public GameObject pauseMenuUI;
    public int mapOrDimensionScene;
    public Button resumeButton;

    public EventSystem eventListener;
    private int currentSceneNumber;

    private void Awake()
    {
        currentSceneNumber = SceneManager.GetActiveScene().buildIndex;
        Debug.Log("SceneNumber " + currentSceneNumber);
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetButtonDown("Pause"))
        {
            if (pauseFlag)
                Resume();
            else
                Pause();
        }
	}

    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        eventListener.SetSelectedGameObject(resumeButton.gameObject);
        Time.timeScale = 0f;
        pauseFlag = !pauseFlag;
        Cursor.visible = true;
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        pauseFlag = !pauseFlag;
        Cursor.visible = false;
    }

    public void RestartLevel()
    {
        pauseFlag = !pauseFlag;
        Time.timeScale = 1f;
        SceneManager.LoadScene(currentSceneNumber);
    }

    public void QuitLevel()
    {
        pauseFlag = !pauseFlag;
        Time.timeScale = 1f;
        SceneManager.LoadScene(2);
        MusicAudioManager.musicAudioManager.ChangeWorldMusic(0);
    }

    public void GoToMap()
    {
        pauseFlag = !pauseFlag;
        Time.timeScale = 1f;

        string currentSceneName = SceneManager.GetActiveScene().name;

        if (currentSceneName.StartsWith("PirateWorld")|| currentSceneName.StartsWith("Tutorial")) {
            SceneManager.LoadScene(5);
            GameManager.instance.MusicControl(8);
        }
        if (currentSceneName.StartsWith("GhostKingdom")) {
            SceneManager.LoadScene(22);
            GameManager.instance.MusicControl(24);
        }
        if (currentSceneName.StartsWith("NeonCity")) {
            SceneManager.LoadScene(33);
            GameManager.instance.MusicControl(35);
        }
        if (currentSceneName.StartsWith("MinimalDimension")) {
            SceneManager.LoadScene(2);
            GameManager.instance.MusicControl(2);
        }
    }

    public void GoToChangeDimensionMap() {
        pauseFlag = !pauseFlag;
        Time.timeScale = 1f;
        SceneManager.LoadScene(4);
        GameManager.instance.MusicControl(2);
    }
}
