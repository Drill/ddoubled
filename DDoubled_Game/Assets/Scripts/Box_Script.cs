﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box_Script : MonoBehaviour {

    private AudioSource BoxSoundEffect;
    private Vector3 lastPosition;
    private BoxCollider boxCollider;
    private float time = 0f;
    private float interpolateTime = .3f;
    [Header("Check this if the Box is at midair at the start of the level")]
    public bool inAir = true;     // true when the box is in air
    public LayerMask groundLayer;   // the layer on which the box produces the bumping sound


    private void Awake()
    {
        lastPosition = transform.position;
        BoxSoundEffect = gameObject.GetComponent<AudioSource>();
        boxCollider = GetComponent<BoxCollider>();
        groundLayer = LayerMask.GetMask("Default");
    }

    private void Update()
    {
        time += Time.deltaTime;
        Vector3 curPos = transform.position;
        if (curPos != lastPosition) 
        {
            if (time >= interpolateTime)
            {
                time = 0f;
            }
        }
        lastPosition = curPos;
    }

    private void OnCollisionEnter(Collision collision) {
        // if the box was in air and now it is on the floor
        if (inAir && Bumped())
        {
            BoxSoundEffect.Play();
            inAir = false;
        }
    }

    private void OnCollisionExit(Collision collision) {
        // if the box was is no more on the floor
        if (collision.gameObject.layer == LayerMask.NameToLayer("Default") && !Bumped()) {
            inAir = true;
        }
    }
    // check if the box has fallen and has bumped the floor
    bool Bumped() {
        // return true if the collision with the specified layer happens vertically
        return Physics.CheckCapsule(boxCollider.bounds.center, new Vector3(boxCollider.bounds.center.x, boxCollider.bounds.min.y, boxCollider.bounds.center.z),
                                    boxCollider.contactOffset * 0.9f, groundLayer);
    }
} 
