﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class InstructionBaseScript : MonoBehaviour
{
    [Header("Time to wait before see the instructions")]
    public float totalWatingTime;
    public Image keyBoard;
    public Image joyStick;
    public Image isometricCorrection;
    public Canvas canvas;
    public GameObject inGameMenu;

    private float waintingTime;
    private bool flag;
    private bool flag2;

    // used to allow the player to see the isometric correction screen after closing the instruction screen
    private float timeBetweenScreens;

    private void Awake()
    {
        waintingTime = totalWatingTime;
        flag = false;
        flag2 = false;
        timeBetweenScreens = 1.0f;
    }
    
    void Update()
    {
        if(!flag && totalWatingTime > 0)
            totalWatingTime -= Time.deltaTime;
        if (flag2 && timeBetweenScreens > 0)
            timeBetweenScreens -= 0.1f;

        // show the instruction screen
        if (totalWatingTime <= 0 && !flag)
        {
            Time.timeScale = 0f;
            canvas.gameObject.SetActive(true);
            if(Input.GetJoystickNames().Length > 0 && !Input.GetJoystickNames().GetValue(0).Equals(""))
            {
                joyStick.gameObject.SetActive(true);
            }
            else
            {
                keyBoard.gameObject.SetActive(true);
            }
            flag = true;
        }

        // close the instruction screen and show the isometric correction screen
        if(flag && Input.GetButtonDown("Pause"))
        {
            joyStick.gameObject.SetActive(false);
            keyBoard.gameObject.SetActive(false);
            isometricCorrection.gameObject.SetActive(true);

            flag2 = true;
        }

        // close the isometric correction screen
        if (timeBetweenScreens <= 0 && Input.GetButtonDown("Pause")) {
            gameObject.SetActive(false);
            inGameMenu.gameObject.SetActive(true);
            Time.timeScale = 1f;
        }
    }
}
