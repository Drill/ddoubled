﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttractionRepellion : MonoBehaviour {
    Rigidbody rb;
    float originalMass;
    bool underMagnetInfluence;
    Transform parentIronObj;    // parent of the iron object when it enters in the range of the manget

    Transform magnetTransform;
    float magnetStrength;
    int magnetDirection;
    bool noGravity;
    //float magnetDistanceStretch;

    // Use this for initialization
    void Awake () {
        rb = GetComponent<Rigidbody>();
        originalMass = rb.mass;
        underMagnetInfluence = false;
	}

    // when the object enters in the magnet's force field, set the parameters
    public void MagnetInfluence(GameObject _magnet, float _magnetStrength, int _magnetDirection, bool _noGravity /*, float _magnetDistanceStretch*/) {
        if (_magnet.CompareTag("Magnet")) {
            //magnetTransform = _magnet.transform.GetChild(1).gameObject.transform; // the pole
			magnetTransform = _magnet.transform;
            magnetStrength = _magnetStrength;
            magnetDirection = _magnetDirection;
            noGravity = _noGravity;
            //magnetDistanceStretch = _magnetDistanceStretch;

            underMagnetInfluence = true;
            parentIronObj = transform.parent;

            if(noGravity && magnetDirection == 1)
                rb.useGravity = false;  // the attraction force of the magnet is able to win against gravity

            rb.mass = magnetStrength * magnetStrength;  // the object's mass increases under the influence of the magnet
        }
    }

    // the object exits from the magnet's force field
    public void NoMoreMagnetInfluence(GameObject _magnet) {
        if (_magnet.CompareTag("Magnet")) {
            // this prevents a bug when the character picks up a sphere under the influence of the magnet
            if (parentIronObj == null && transform.parent != null)
                return;

            underMagnetInfluence = false;
            if(noGravity)
                rb.useGravity = true;   // restore gravity

            rb.mass = originalMass; // restore the value
        }
    }

    public void PolarityInversion() {
        magnetDirection *= -1;
        if(noGravity)
            rb.useGravity = !rb.useGravity; // this allows to have the gravity disabled(enabled) when the magnet switches from repellion(attraction) mode to attraction(repellion) mode
    }
    
    void FixedUpdate () {
        if (underMagnetInfluence) {
            // apply a force to the iron object. The direction depends on the magnet's mode and the intensity depends on the distance between this object and the magnet
            //Vector3 directionToMagnet = magnetTransform.position - transform.position;
            //float distance = Vector3.Distance(magnetTransform.position, transform.position);
            //float magnetDistanceStr = (magnetDistanceStretch / distance) * magnetStrength;
            //rb.AddForce(magnetDistanceStr * (directionToMagnet * magnetDirection), ForceMode.Acceleration);

            // apply a translation to the iron object. The direction depends on the magnet's mode
            if (transform.parent != null) {
                Transform characterTransform = transform.parent.gameObject.transform;    // the character

                characterTransform.position = Vector3.MoveTowards(characterTransform.position, magnetTransform.position, magnetDirection * magnetStrength * Time.fixedDeltaTime);
            }
            else
                transform.position = Vector3.MoveTowards(transform.position, magnetTransform.position, magnetDirection * magnetStrength * Time.fixedDeltaTime);
        }
    }

    public bool isUnderMagnetInfluence() {
        return underMagnetInfluence;
    }
}
