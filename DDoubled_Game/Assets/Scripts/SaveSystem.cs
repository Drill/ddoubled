﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class SaveSystem
{
   public static void SaveData(GameManager gameManager)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/ddoubleData.data";
        Debug.Log("SavedFileLocation: "+ path);
        FileStream stream = new FileStream(path, FileMode.Create);

        StoredDataScript data = new StoredDataScript(gameManager);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static StoredDataScript LoadData()
    {
        string path = Application.persistentDataPath + "/ddoubleData.data";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            StoredDataScript result = formatter.Deserialize(stream) as StoredDataScript;
            stream.Close();

            return result;
        }
        else
        {
            Debug.Log("Saved file not found");
            return null;
        }
    }

}
