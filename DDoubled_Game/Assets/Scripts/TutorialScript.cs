﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class TutorialScript : MonoBehaviour
{
    public VideoPlayer videoPlayer;
    public GameObject containers;

    private bool pause;

    private void OnEnable()
    {
        EventManager.StartListening("NextLevel", NextLevel);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            containers.active = true;
            if(videoPlayer.isPrepared)
                videoPlayer.Play();
        }
    }

    private void Update()
    {
        if(PauseMenu.pauseFlag && !pause && containers.active)
        {
            videoPlayer.Pause();
            pause = !pause;
        }else if(!PauseMenu.pauseFlag && pause && containers.active)
        {
            videoPlayer.Play();
            pause = !pause;
        }
    }

    private void NextLevel()
    {
        EventManager.StopListening("NextLevel", NextLevel);
        containers.active = false;
    }
}
