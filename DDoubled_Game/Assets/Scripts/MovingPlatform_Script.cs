﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform_Script : MonoBehaviour {
    [Header("Define which is the starting position")]
    public Transform startPoint;
    private Vector3 startPos_Original;
    [Header("Define which is the end position")]
    public Transform endPoint;
    private Vector3 endPos_Original;

    public float speed = 3;
    private bool isMoving;      // true if the platform is actually moving
    private int direction;  // 1 if the platform moves towards the endPoint; -1 if it moves towards the startPoint

    [Header("Check this if the platform starts attached to the floor or a wall")]
    public bool startsAttachedToSomething;

    private List<GameObject> objectsOnPlatform = new List<GameObject>();


    void Awake() {
        // Store the original values of startPoint and endPoint
        startPos_Original = startPoint.position;
        endPos_Original = endPoint.position;

        isMoving = false;
        direction = -1;  // by default the value is -1 because the platform is at its startPoint
    }

    public void MovePlatform() {
        direction *= -1;    // in this way the platform starts to move towards the endPoint or towards the startPoint
    }

    void OnTriggerEnter(Collider other) {
        if(other.gameObject.transform.parent == null) {
            objectsOnPlatform.Add(other.gameObject);
            other.transform.parent = transform;
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.gameObject.transform.parent == transform) {
            objectsOnPlatform.Remove(other.gameObject);
            other.transform.parent = null;
        }
    }

    void FixedUpdate() {
        //  if the platform doesn't start attached to the floor or a wall
        if (!startsAttachedToSomething) {
            // if the platform has to move towards the endPoint, but it isn't still there and if there aren't obstacles on the path
            if ((direction == 1 && transform.position != endPos_Original) && this.GetComponentInChildren<ObstacleDetector_Script>().NumberOfObjectsInRange() < 1
                ||
                // if the platform has to move towards the startPoint, but it isn't still there
                (direction == -1 && transform.position != startPos_Original)) {
                isMoving = true;

            } else
                isMoving = false;

        //  if the platform starts attached to the floor or a wall
        } else {
            // if the platform has to move towards the endPoint, but it isn't still there
            if ((direction == 1 && transform.position != endPos_Original)
            ||
            // if the platform has to move towards the startPoint, but it isn't still there and if there aren't obstacles on the path
            (direction == -1 && transform.position != startPos_Original) && this.GetComponentInChildren<ObstacleDetector_Script>().NumberOfObjectsInRange() < 1) {

                isMoving = true;

            } else
                isMoving = false;
        }


        if (direction == 1 && isMoving)
            this.gameObject.transform.position = Vector3.MoveTowards(this.gameObject.transform.position, endPos_Original, speed * Time.deltaTime);
        
        if (direction == -1 && isMoving)
            this.gameObject.transform.position = Vector3.MoveTowards(this.gameObject.transform.position, startPos_Original, speed * Time.deltaTime);
    }
}
