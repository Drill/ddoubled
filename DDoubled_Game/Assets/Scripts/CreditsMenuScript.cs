﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsMenuScript : MonoBehaviour
{

    public GameObject mainMenu;
    public GameObject creditsMenu;
    public Animator anim;

    public void ChangeCreditMusic(bool flag)
    {
        if (flag)
            MusicAudioManager.musicAudioManager.ChangeWorldMusic(5);
        else
            MusicAudioManager.musicAudioManager.ChangeWorldMusic(0);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ChangeCreditMusic(false);
            mainMenu.active = true;
            creditsMenu.active = false;
        }

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("End"))
        {
            ChangeCreditMusic(false);
            mainMenu.active = true;
            creditsMenu.active = false;
        }
    }
}
