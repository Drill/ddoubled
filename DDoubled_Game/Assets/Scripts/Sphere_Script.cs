﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere_Script : MonoBehaviour {

   
    private Transform hand;
    public Rigidbody sphere_Rigidbody;
    public bool playerDetected;
    public bool sphereTaken;
    public GameObject player;


    // Use this for initialization
    private void Awake()
    {

        sphere_Rigidbody = GetComponent<Rigidbody>();
        sphere_Rigidbody.useGravity = true;
        sphereTaken = false;
        playerDetected = false;
        hand = null;
        player = null;


    }

    // Update is called once per frame
    private void Update()
    {
        if (sphereTaken)
        {
            transform.position = hand.position;
            transform.rotation = hand.rotation * Quaternion.AngleAxis(90, new Vector3(-1, 0, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));

        }
        else if (!sphereTaken)
        {
            if (!sphere_Rigidbody.useGravity)
            {
                hand = null;
            }
        }
    }

    public void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Player"))
        {
            player = other.gameObject;
            playerDetected = true;
        }

    }

    private void OnCollisionExit(Collision other)
    {
        if (other.collider.CompareTag("Player"))
        {
            playerDetected = false;

        }
        
    }

    public Transform getHandTransform()
    {
        return hand;
    }

    internal void setHandTransform(Transform newOne)
    {
        hand = newOne;
    }
}
