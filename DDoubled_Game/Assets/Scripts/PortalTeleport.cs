﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTeleport : MonoBehaviour {
    
    AudioSource teleportSound;
    
    public GameObject spawnPoint;
    public bool isEntrance;    // if something can enter in this portal
    public float launchStrength = 4;    // force applied to the object that exits from a portal
    bool justUsed;  // two objects can't enter in the same portal at the same time
    float rechargeTime; // time to wait in order to pass again through the portal

    Material[] portalMaterials;
    [Header("Material used when is possible to enter in the portal")]
    public Material portalEntranceMaterial;
    [Header("Material used when is not possible to enter in the portal")]
    public Material portalExitMaterial;
    int materialToUse;

    // Use this for initialization
    void Awake () {
        teleportSound = GetComponent<AudioSource>();

        justUsed = false;
        rechargeTime = 0f;  // the portal can be used

        portalMaterials = new Material[2] ;
        portalMaterials[0] = portalExitMaterial;
        portalMaterials[1] = portalEntranceMaterial;
        materialToUse = 0;
    }

    // when an object enters in the portal
    void OnTriggerStay(Collider other) {
        if (isEntrance && other.gameObject.transform.parent == null && !other.isTrigger && !justUsed && rechargeTime <= 0) {
            justUsed = true;    // lock the portal for the current object
            other.gameObject.transform.position = spawnPoint.transform.position;
            teleportSound.Play();
            
            // a little force is applied to the object that exits from a portal
            other.GetComponent<Rigidbody>().AddForce(launchStrength * (spawnPoint.transform.GetChild(0).gameObject.transform.position - other.transform.position), ForceMode.Impulse);
            spawnPoint.GetComponent<PortalTeleport>().rechargeTime = 1.5f; // you can't enter immediately in the exit portal
        }
    }

    public void ToggleIsEntry() {
        // toggle the aspect of the portal
        materialToUse = (materialToUse + 1) % 2;
        this.GetComponent<MeshRenderer>().material = portalMaterials[materialToUse];

        isEntrance = !isEntrance;
    }

    // Update is called once per frame
    void Update () {
        justUsed = false;   // another object can enter in this portal

        if (rechargeTime > 0)
            rechargeTime -= Time.deltaTime;
	}
}
