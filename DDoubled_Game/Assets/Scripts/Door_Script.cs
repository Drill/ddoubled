﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door_Script : MonoBehaviour {

   
    
    [Header("Array of Locks")] public List<GameObject> locks = new List<GameObject>();
    [Header("0 rotate with axis x, 1 rotate with axis y, 2 door go down")] public int rotationAxis = 1;
    [Header("direction of the rotation: Clockwise +1, antiClockwise -1")] public int clock = 1;
    private int NumberOfKey = 0;
    private AudioSource DoorSoundEffect;
    private Animator door_Animator;
    private int rotation=0;
    private float initial_degrees;
    private float final_degrees;
    private float yDegree;
    private float zDegree;
    private float actual_position;

    [Header("Request Door Sounds")]
    public AudioClip unlockSound;
    public AudioClip openDoorSound;


    private void Awake() {
        DoorSoundEffect = GetComponent<AudioSource>();
        door_Animator = GetComponent<Animator>();
        NumberOfKey = locks.Count;

        if (rotationAxis == 1)
        {

            initial_degrees = transform.rotation.eulerAngles.y;
            final_degrees = initial_degrees + 90 * clock;
        }
        else if (rotationAxis == 0)
        {
            initial_degrees = transform.rotation.eulerAngles.x;
            yDegree = transform.rotation.eulerAngles.y;
            zDegree = transform.rotation.eulerAngles.z;
            final_degrees = initial_degrees + 90 * clock;
        }
        else if (rotationAxis == 2)
        {
            actual_position = 0;
        }
    }
	

	void Update () {
        if (rotationAxis == 1)
        {
            if (rotation == 1)
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, final_degrees, 0), 50f * Time.deltaTime);
                
            else if (rotation == -1)
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, initial_degrees, 0), 50f * Time.deltaTime);
             
        }
        else if (rotationAxis == 0)
        {
            if (rotation == 1)
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(final_degrees, yDegree, zDegree), 50f * Time.deltaTime);
             
            else if (rotation == -1)
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(initial_degrees, yDegree, zDegree), 50f * Time.deltaTime);
        }
        else if (rotationAxis == 2)
        {
            float time;
            time = 3 * Time.deltaTime;
            

            if (rotation == 1 && actual_position > -4)
            {
                actual_position -= time;
                transform.Translate(0, 0, -time);
            }

            else if (rotation == -1 && actual_position < 0)
            {
                actual_position += time;
                transform.Translate(0, 0, time);
            }
        }


    }

    public void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Key")) {
            OpenDoor(collider.gameObject);
        }

    }

    public void OpenDoor()
    {
        rotation = 1;
        if (!DoorSoundEffect.isPlaying)
        {
            DoorSoundEffect.Stop();
            DoorSoundEffect.PlayOneShot(openDoorSound);
        }
    }

    public void OpenDoor(GameObject obj)
    {
        if (NumberOfKey > 0)
        {
            NumberOfKey--;
            Destroy(locks[NumberOfKey]);
            locks.RemoveAt(NumberOfKey);
            Destroy(obj);
            DoorSoundEffect.PlayOneShot(unlockSound);
            if (NumberOfKey == 0)
            {
                OpenDoor(); 
            }
        }
    }

    public void CloseDoor()
    {
        rotation = -1;
        if (!DoorSoundEffect.isPlaying)
        {
            DoorSoundEffect.Stop();
            DoorSoundEffect.PlayOneShot(openDoorSound);
        }
    }
}
