﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressablePlatformScript : MonoBehaviour {
    public List<GameObject> objsToActivate;
    int objsOn; // number of objects on the pressable platform
    private AudioSource pressablePlatformSoundSource;

    void Awake() {
        objsOn = 0;
        pressablePlatformSoundSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other) {
        try {
            // If "other" is able to press the pressable platform
            if (!other.isTrigger && other.GetComponent<Rigidbody>().mass >= 1) {
                objsOn += 1;

                // If the pressable platform wasn't already pressed
                if (objsOn == 1) {
                    // It is visually pressed
                    this.gameObject.transform.position -= Vector3.up * 2 * Time.deltaTime;
                    this.gameObject.GetComponent<BoxCollider>().center += new Vector3(0, 0, 0.05f);

                    if (!pressablePlatformSoundSource.isPlaying)
                        pressablePlatformSoundSource.Play();

                    foreach (GameObject obj in objsToActivate) {
                        switch (obj.tag) {
                            case "Elevator":
                                obj.GetComponent<Elevator_Script>().EnableElement();
                                break;
                            case "Magnet":
                                obj.GetComponent<Magnet_Script>().InvertDirection();
                                break;
                            case "ConveyorBelt":
                                obj.GetComponent<ConveyorBelt_Script>().ChangeDirection();
                                break;
                            case "Door":
                                obj.GetComponent<Door_Script>().OpenDoor();
                                break;
                            case "RotatingWall":
                                obj.GetComponent<Door_Script>().OpenDoor();
                                break;
                            case "MovingPlatform":
                                obj.GetComponent<MovingPlatform_Script>().MovePlatform();
                                break;
                            case "RotatingPlatform":
                                obj.GetComponent<RotatingPlatform>().EnableElement();
                                break;
                            case "Cage":
                                obj.GetComponent<Cage_Script>().CageFall();
                                break;
							case "Moving_wall":
								obj.GetComponent<Moving_walls>().EnableElement();
								break;
                            case "Portal":
                                obj.GetComponent<PortalTeleport>().ToggleIsEntry();
                                break;
                        }
                    }
                }
            }
        } catch (MissingComponentException e) {
            // The other collider has no Rigidbody attached. Ignore it.
            Debug.Log("An object without a rigidbody has tried to press the pressable platform" + gameObject.name);
        }
    }

    void OnTriggerExit(Collider other) {
        try {
            // If something able to press the pressable platform leaves
            if (!other.isTrigger && other.GetComponent<Rigidbody>().mass >= 1) {
                objsOn -= 1;

                // The pressable platform is no more pressed
                if (objsOn == 0) {
                    // It is visually released
                    this.gameObject.transform.position += Vector3.up * 2 * Time.deltaTime;
                    this.gameObject.GetComponent<BoxCollider>().center -= new Vector3(0, 0, 0.05f);

                    if (!pressablePlatformSoundSource.isPlaying)
                        pressablePlatformSoundSource.Play();

                    foreach (GameObject obj in objsToActivate) {
                        switch (obj.tag) {
                            case "Elevator":
                                obj.GetComponent<Elevator_Script>().DisableElement();
                                break;
                            case "Magnet":
                                obj.GetComponent<Magnet_Script>().InvertDirection();
                                break;
                            case "ConveyorBelt":
                                obj.GetComponent<ConveyorBelt_Script>().ChangeDirection();
                                break;
                            case "Door":
                                obj.GetComponent<Door_Script>().CloseDoor();
                                break;
                            case "RotatingWall":
                                obj.GetComponent<Door_Script>().CloseDoor();
                                break;
                            case "MovingPlatform":
                                obj.GetComponent<MovingPlatform_Script>().MovePlatform();
                                break;
                            case "RotatingPlatform":
                                obj.GetComponent<RotatingPlatform>().DisableElement();
                                break;
                            case "Moving_wall":
                                obj.GetComponent<Moving_walls>().DisableElement();
                                break;
                            case "Portal":
                                obj.GetComponent<PortalTeleport>().ToggleIsEntry();
                                break;
                        }
                    }
                }
            }
        } catch (MissingComponentException e) {
            // The other collider has no Rigidbody attached. Ignore it.
            Debug.Log("An object without a rigidbody has tried to release the pressable platform");
        }
    }
}
