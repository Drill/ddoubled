﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectAudioManager : MonoBehaviour
{
    private AudioSource audioSource;
    private bool soundDecrase = false;

    // Start is called before the first frame update
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (PauseMenu.pauseFlag && !soundDecrase)
        {
            audioSource.Pause();
            soundDecrase = !soundDecrase;
        }
        else if (!PauseMenu.pauseFlag && soundDecrase)
        {
            audioSource.Play();
            soundDecrase = !soundDecrase;
        }
    }

}
