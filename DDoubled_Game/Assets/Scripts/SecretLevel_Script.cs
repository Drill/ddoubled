﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SecretLevel_Script : MonoBehaviour {
    public static bool changingScene;
    public bool insideSecretLevel;

    void Awake() {
        changingScene = false;
    }

    public void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            changingScene = true;

            if (!insideSecretLevel) {
                SceneManager.LoadScene(40);
                GameManager.instance.MusicControl(40);
            } else {
                SceneManager.LoadScene(3);
                GameManager.instance.MusicControl(3);
            }
        }
    }
}
