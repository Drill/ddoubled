﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map_PlayerMovements : MonoBehaviour {

    //Parameters to manage the player exect sounds
    [Header("Player sound source end clips")]
    public AudioSource playerSoundSource;
    public AudioClip footstepSound;

    //Fixed time that the player must attend to do a static animation and that doesn't change
    [Header("Wating time to activate some static animation")]
    public float waitingTime = 5f;

    private Animator anim;
    private Rigidbody playerRigidbody;
    //Timer that is decreamented each time that the player is in static position
    private float countdown;
    private CameraControl camera;
    private bool isMoving;
    
    private void Awake()
    {
        UnityEngine.Random.InitState(System.DateTime.Now.Millisecond);
        playerRigidbody = GetComponent<Rigidbody>();
        camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraControl>();
        anim = GetComponent<Animator>();
        ResetTime();
        playerRigidbody.freezeRotation = true;
        isMoving = false;
    }

    private void FixedUpdate() {
        if (!isMoving) {
            SetStatic();
            if ((anim.GetCurrentAnimatorStateInfo(0).IsName("CharacterIdle")))
            {
                countdown -= Time.deltaTime;
                if (countdown <= 0)
                    //StaticAnimation(UnityEngine.Random.Range(0, 10));
                    StaticAnimation(1);
            }
        } else {
            ResetTime();
            WalkingOrPushingWithFreeHand();
        }
    }

    public void SetIsMoving(bool newValue) {
        isMoving = newValue;
    }

    private void ResetTime()
    {
        countdown = UnityEngine.Random.Range(5, 50);
    }

    private void WalkingOrPushingWithFreeHand()
    {
        ResetStaticAnimation();
        
        anim.SetBool("IsWalking", true);
        if (!playerSoundSource.isPlaying)
            playerSoundSource.PlayOneShot(footstepSound);
    }

    private void ResetStaticAnimation()
    {
        if (anim.GetBool("StaticAnimation")) { 
            anim.SetBool("StaticAnimation", false);
            playerSoundSource.Stop();
            playerSoundSource.transform.position = new Vector3(0f, 0f, 0f);
        }
    }

    private void StaticAnimation(int animation)
    {
        /*animation = animation * animation;
        if(animation > 0 && animation < 20)
        {
            playerSoundSource.transform.position = new Vector3(0f, 1f, 0f);
            playerSoundSource.clip = stomachSound;
            playerSoundSource.Play();
        }
        else if(animation >= 20 && animation < 60)
        {
            Invoke("SetStatic", 1.8f);
            anim.SetBool("StaticAnimation", true);
        }
        else if(animation >= 60 && animation < 100)
        {
            Invoke("SetStatic", 2f);
            anim.SetBool("StaticAnimation1", true);
        }*/
        Invoke("SetStatic", 1.8f);
        anim.SetBool("StaticAnimation", true);
        playerSoundSource.transform.position = new Vector3(0f, 1f, 0f);
        playerSoundSource.Play();
    }

    private void SetStatic()
    {
        if (anim.GetBool("IsWalking"))
        {
            anim.SetBool("IsWalking", false);
        }
        else if (anim.GetBool("StaticAnimation"))
            anim.SetBool("StaticAnimation", false);
        
        countdown = waitingTime;
        playerSoundSource.Stop();
        playerSoundSource.transform.position = new Vector3(0f, 0f, 0f);
    }
}
