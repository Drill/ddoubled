﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicAudioManager : MonoBehaviour
{

    public static MusicAudioManager musicAudioManager { get; private set; }
    private static AudioSource musicSource;
    private bool soundDecrase = false;

    [Header("Music Clips")]
    public AudioClip menuMusic;
    public AudioClip pirateWorldMusic;
    public AudioClip neonCityMusic;
    public AudioClip ghostKingdomMusic;
    public AudioClip minimalDimensionMusic;
    public AudioClip creditsMusic;


    // Use this for initialization
    private void Awake()
    {
        if (musicAudioManager == null)
        {
            musicAudioManager = this;
            musicSource = GetComponent<AudioSource>();
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    public void Update()
    {
        if (PauseMenu.pauseFlag && !soundDecrase)
        {
            musicSource.Pause();
            soundDecrase = !soundDecrase;
        }
        else if (!PauseMenu.pauseFlag && soundDecrase)
        {
            musicSource.Play();
            soundDecrase = !soundDecrase;
        }
    }

    public void ChangeWorldMusic(int v)
    {
        Debug.Log("ChangeMusic");
        switch (v)
        {
            case 0:
                musicSource.Stop();
                musicSource.clip = menuMusic;
                musicSource.Play();
                break;
            case 1:
                musicSource.Stop();
                musicSource.clip = pirateWorldMusic;
                musicSource.Play();
                break;
            case 2:
                musicSource.Stop();
                musicSource.clip = ghostKingdomMusic;
                musicSource.Play();
                break;
            case 3:
                musicSource.Stop();
                musicSource.clip = neonCityMusic;
                musicSource.Play();
                break;
            case 4:
                musicSource.Stop();
                musicSource.clip = minimalDimensionMusic;
                musicSource.Play();
                break;
            case 5:
                musicSource.Stop();
                musicSource.clip = creditsMusic;
                musicSource.Play();
                break;
        }
    }

    internal void stopMusic()
    {
        musicSource.Stop();
    }
}
