﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class TurialAtStartWithTimeScript : MonoBehaviour
{

    public VideoPlayer videoPlayer;
    public GameObject containers;
    public RawImage im;
    public float totalTimeForTutorial;
    public bool pause = false;

    private float countDown;

    private void Awake()
    {
        countDown = totalTimeForTutorial;
    }

    private void Update()
    {
        if (!PauseMenu.pauseFlag && videoPlayer.isPrepared && !videoPlayer.isPlaying && im.texture != null)
        {
            videoPlayer.Play();
            containers.active = true;
        }
            
        if (containers.active && videoPlayer.isPlaying)
        { 
            countDown -= Time.deltaTime;
            if (countDown <= 0)
                containers.active = false;
        }

        if (PauseMenu.pauseFlag && videoPlayer.isPlaying)
            videoPlayer.Pause();       

    }

    private void OnEnable()
    {
        EventManager.StartListening("NextLevel", NextLevel);
    }


    private void NextLevel()
    {
        EventManager.StopListening("NextLevel", NextLevel);
        containers.active = false;
    }
}
