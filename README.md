# DDoubled

Game developed during the course "Videogame design and programming" at Politecnico di Milano. <br/>
The game can be downloaded also at: https://polimi-game-collective.itch.io/ddoubled. Please note that the Linux version at this link is not the updated version that you can find in this repository.

## Description
Solve amazing puzzles with the help of many of you! Travel to different wonderful worlds. All to return home! Each level consists of intriguing puzzles where you have to reach the door in order to continue your travel. Only one of you could not be enough, but remember: you are a scientist and one of your experiments could help you. Look, a DDouble! That surely will help you! So, run to take it and….

DDoubled is a 3D puzzle game in which you can control multiple characters at the same time and they will execute the same actions, at the same time! <br/>
Solve puzzles by pulling levers, pushing boxes, using portals and even more! Exploit the scene itself in order to manage the disposition of the characters!

## Trailer
Here you can find the trailer of the first release: https://www.youtube.com/watch?v=iUdhKycy4vg.

## Screenshots
![DDoubled__Screenshot1](https://gitlab.com/Drill/DDoubled/uploads/4660a96600429d078a100ac268db7894/1-PW4.png)
![DDoubled__Screenshot2](https://gitlab.com/Drill/DDoubled/uploads/4a7494fecd408b1c0101bd43cc45a832/2-GKP_3.png)
![DDoubled__Screenshot3](https://gitlab.com/Drill/DDoubled/uploads/ade8c3d5c19184f34b0ce1701bb367fe/3-NC6.png)
![DDoubled__Screenshot4](https://gitlab.com/Drill/DDoubled/uploads/d065a86ac28eb22bd533c1dd20fe3977/4-GKP_2.png)
![DDoubled__Screenshot5](https://gitlab.com/Drill/DDoubled/uploads/37be7bae91d08f55b0ffade1c0385851/5-GK1_1.png)
![DDoubled__Screenshot6](https://gitlab.com/Drill/DDoubled/uploads/be01ecd5d43bc92e46a1c8d3104392c4/6-PWP_2.png)
![DDoubled__Screenshot7](https://gitlab.com/Drill/DDoubled/uploads/c7751e9687401e79146bf64de96749bb/7-NCP_3.png)
<br/><br/>

## Contributors:
* Rosa Matteo (programmer)
* Virciglio Salvatore Cesare Giuliano (programmer)
* Grilli Tommaso (programmer)
* Cattaneo Andrej (designer)
* Gennaioli Michele (programmer)